var parameters;
var fMapMarker = null;

$(function() {
    $.get({
        url: "/receiptGather",
        dataType: "json"
    })
     .done(function(result) {
         var rTable = createTable(window.receiptList = result['receipts']);
         $('.receiptTable').append(rTable);
         $(window).resize(function(event) {
             $('.receiptTable').empty().append(createTable(
                 window.receiptList
             ));
         });
     })
     .fail(function() {
         console.error("Failed to gather receipts");
     });
});

function createTable(receipts) {
  var numCols = Math.floor($('.receiptTable').width() / 100);
  var $table = $('<table class="rTable">')
  $table
    .css('border-collapse', 'separate')
    .css('border-spacing',
      (($('.receiptTable').width() - (numCols * 100)) / (numCols + 1)) + 'px')
    ;
  
  var $row;
  $.each(receipts, function(j, item) {
    if (j % numCols == 0) {
      $row = $('<tr>').appendTo($table);
    }
    var $receipt = $('<td class="receiptLink">').appendTo($row)
    var receiptLine = function() {return $('<div>').appendTo($receipt);};
    $receipt.data('id', item.Id);
    if (item.OtherOwner != "") {
      $receipt.addClass('foreignReceipt');
    }
    receiptLine().text(item.Date);
    if (item.OtherOwner.indexOf('@') >= 0) {
      receiptLine().append(
        $('<span>').text(
          item.OtherOwner.substr(0, item.OtherOwner.indexOf('@'))
        )
      );
    } else {
      receiptLine().text('\xA0');
    }
    receiptLine().text(item.Account || '\xA0');
    receiptLine().text(item.Total == "" ? "[unset]" : "$" + item.Total);
  });
  return $table;
}

/* Utility function to convert a canvas to a BLOB

Taken from http://christopher5106.github.io/web/2015/12/13/HTML5-file-image-upload-and-resizing-javascript-with-progress-bar.html
*/
var dataURLToBlob = function(dataURL) {
  console.log("DataURLToBlob")
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = parts[1];

        return new Blob([raw], {type: contentType});
    }

    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: contentType});
}

function createReceipt(fileInput)
{
    var maxSmallDim = 1200;
    var receiptBlob = fileInput.files[0];
    var swimlanes = [], formData = {};
    
    var $uploadModal = $('#receiptCaptureProgressModal');
    $uploadModal.modal({
      escapeClose: false,
      clickClose: false,
      showClose: false
    });
    
    var dataUrl, loadedImage;
    (new AsyncChain())
        .add(function(chain) {
            $.readFileToDataURLAsync(receiptBlob)
                .done(function(value) {
                    dataUrl = value;
                    chain.executeNext();
                })
                .fail(function(error) {
                    window.alert("Unable to read image data");
                })
                ;
        })
        .add(function(chain) {
            $.loadImageFromUrlAsync(dataUrl)
                .done(function(image) {
                    loadedImage = image;
                    chain.executeNext();
                })
                .fail(function(error) {
                    window.alert("Unable to load image for processing");
                })
                ;
        })
        .add(function (chain) {
            if (!EXIF.getData(receiptBlob, function() {
                loadedImage.orientation = EXIF.getTag(this, "Orientation");
                chain.executeNext();
            })) {
                console.log("Failed to read EXIF data");
                chain.executeNext();
            }
        })
        .add(function (chain) {
            /*
            Adapted from http://christopher5106.github.io/web/2015/12/13/HTML5-file-image-upload-and-resizing-javascript-with-progress-bar.html
            and
            http://stackoverflow.com/a/37750456/160072
            */
            var canvas = document.createElement('canvas'),
                width = loadedImage.width,
                height = loadedImage.height,
                scale = 1;
            var iWidth = width, iHeight = height;
            
            if (width < height) {
                if (maxSmallDim < width) {
                    scale = maxSmallDim / width;
                    height *= scale;
                    width = maxSmallDim;
                }
            } else {
                if (maxSmallDim < height) {
                    scale = maxSmallDim / height;
                    width *= scale;
                    height = maxSmallDim;
                }
            }
            
            
            if (loadedImage.orientation && loadedImage.orientation > 4) {
                var temp = width;
                width = height;
                height = temp;
            }
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext('2d');
            ctx.save();
            ctx.scale(scale, scale);
            switch (loadedImage.orientation) {
              case 2: ctx.translate(iWidth, 0);      ctx.scale(-1, 1); break;
              case 3: ctx.translate(iWidth, iHeight); ctx.rotate(Math.PI); break;
              case 4: ctx.translate(0, iHeight);     ctx.scale(1, -1); break;
              case 5: ctx.rotate(0.5 * Math.PI);    ctx.scale(1, -1); break;
              case 6: ctx.rotate(0.5 * Math.PI);    ctx.translate(0, -iHeight); break;
              case 7: ctx.rotate(0.5 * Math.PI);    ctx.translate(iWidth, -iHeight); ctx.scale(-1, 1); break;
              case 8: ctx.rotate(-0.5 * Math.PI);   ctx.translate(-iWidth, 0); break;
            }
            ctx.drawImage(loadedImage, 0, 0);
            ctx.restore();
            var dataUrl = canvas.toDataURL('image/jpeg');
            receiptBlob = dataURLToBlob(dataUrl);
            chain.executeNext();
        })
        .promiseTo(function(promise) {swimlanes.push(promise);})
        .executeNext()
        ;
    
    swimlanes.push($.allowFailure($.geolocateAsync({timeout: 1500, maximumAge: 120000})
        .done(function(coords) {
            formData.lat = coords.latitude;
            formData.lon = coords.longitude;
            formData.locParams = "&lat=" + formData.lat + "&lon=" + formData.lon;
        })
        .fail(function(error) {
            console.log("Geolocation error: " + error);
        })
    ));
    
    $.when.apply($, swimlanes)
        .done(function() {
            var reqBody = new FormData();
            if (formData.lat && formData.lon) {
              reqBody.append("lat", formData.lat);
              reqBody.append("lon", formData.lon);
            }
            reqBody.append("receiptImage", receiptBlob);
            
            // Based on http://stackoverflow.com/a/5976031/160072
            $.ajax({
              url: "/Receipt/new",
              data: reqBody,
              xhr: customXhr({
                uploadProgress: function(progress) {
                  if (progress.lengthComputable) {
                    $uploadModal.find('progress')
                      .prop('max', progress.total)
                      .val(progress.loaded)
                      ;
                  }
                }
              }),
              cache: false,
              contentType: false,
              processData: false,
              method: 'POST',
              dataType: 'text'
            })
              .done(function(result) {
                window.location.assign(result);
              })
              .fail(function() {
                $uploadModal.close();
                window.alert("Receipt creation failed.");
              })
              ;
        })
        ;
}

//Google Authentication Functions
function signOut()
{
    //gapi.auth2.init().then(function() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function() {
            window.location.assign("/signout");
        });
    //});
}

function gOnLoad()
{
    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
}

$(document).on('click', '.receiptLink', function(){
    var id = $(this).data("id");
    window.location.assign(receiptURI(id));
});

function openDropdown() {
    document.getElementById('actionDropdown').classList.toggle('show');
}

window.onclick = function(event) {
    if(!event.target.matches('.dropBtn')) {
	var dropdowns = document.getElementsByClassName('dropdown-content');
	for(var i = 0; i < dropdowns.length; i++) {
	    var openDropdown = dropdowns[i];
	    if(openDropdown.classList.contains('show')) {
		openDropdown.classList.remove('show');
	    }
	}
    }
}

function filter() {
    var filterData = {
        costMin: $('#fCostMin').val(),
        costMax: $('#fCostMax').val(),
        distance: $('#fDistance').val(),
        startDate: $('#fDateStart').val(),
        endDate: $('#fDateEnd').val(),
        order: $('input[name=fOrder]:checked', '#filterOptions').val()
    };
    var gotLocation = false;
    getFilterLocationCoordsAsync(filterData.distance != "")
        .done(function(coords) {
            filterData.latitude = coords.latitude;
            filterData.longitude = coords.longitude;
            gotLocation = true;
        })
        .always(function() {
            if (!gotLocation && filterData.distance != "") {
                filterData.distance = "";
                window.alert("Unable to filter by location.");
            }
            $.ajax({
                method: "GET",
                url: "/receiptFilter",
                data: filterData,
                dataType: 'json'
            })
                .done(function(result) {
                    var $receiptTable = $('.receiptTable');
                    $receiptTable.empty();
                    var rTable = createTable(window.receiptList = result['receipts']);
                    $receiptTable.append(rTable);
                })
                .fail(function() {
                    window.alert("Filter failed.");
                })
                ;
        })
        ;
}

function distanceFilterHandler(selection) {
    var dMethod = document.getElementById('fDistanceMethod');
    
    while(dMethod.firstChild) {
        dMethod.removeChild(dMethod.firstChild);
    }
    dMethod.setAttribute("style", "");
    
    if(selection.value == "address") {
        dMethod.setAttribute("style", "width:auto; height: auto; margin:10px 0 10px 0");
        dMethod.innerHTML = "<p>Address:</p><input type='text' placeholder='Enter address here' id='fAddress' style='margin:10px 0 0 10px;'>";
    }
    else if(selection.value == "customPoint"){
        var myLatLng = new google.maps.LatLng(0.0, 0.0);
        
        var myOptions = {
            zoom: 10,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        dMethod.setAttribute("style", "height:250px; width:250px; margin:10px 0 10px 0;");
        map = new google.maps.Map(document.getElementById('fDistanceMethod'), myOptions);

        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(myLatLng);
            }, function(error) {
                console.log("Geolocation error: " + error);
            });
        }
        
        google.maps.event.addListener(map, 'click', function(event) {
            if(fMapMarker != null) {
                fMapMarker.setMap(null);
            }

            fMapMarker = new google.maps.Marker({
                position: event.latLng,
                map: map
            });
            //map.setCenter(event.latLng);
        });
    }
}

function getFilterLocationCoordsAsync(shouldTryHard) {
    return $.Deferred(function(op) {
        var sMethod = $('#distanceMethod').val();
        if (sMethod == "myLocation") {
            $.geolocateAsync({timeout: 1500, maximumAge: 120000})
                .done(function(coords) {
                    op.resolve(coords);
                })
                .fail(function() {
                    op.reject();
                })
                ;
        }
        else if(sMethod == "address") {
            if (shouldTryHard) {
                geocoder = new google.maps.Geocoder();
                var address = $('#fAddress').val();
                geocoder.geocode( { 'address': address }, function(results, status) {
                    if(status == 'OK') {
                        op.resolve({
                            latitude: results[0].geometry.location.lat(),
                            longitude: results[0].geometry.location.lng()
                        });
                    } else {
                        console.log("Geocode was unsuccessful: " + status);
                        op.reject();
                    }
                });
            } else {
                // Getting an address if we "shouldn't try hard" uses up
                // limited resources -- so just refuse to look.  shouldTryHard
                // is about having a distance to work with.
                op.reject();
            }
        }
        else {
            if(fMapMarker != null) {
                op.resolve({
                    latitude: fMapMarker.getPosition().lat(),
                    longitude: fMapMarker.getPosition().lng()
                });
            } else {
                op.reject();
            }
        }
    }).promise();
}
