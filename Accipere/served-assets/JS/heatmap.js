var map, heatmap, bounds;

function initMap()
{
    receipts = JSON.parse(localStorage.getItem('receipts'));
    $.get({
        url: "/receiptGather",
        dataType: "json"
    })
     .done(function(result) {
         bounds = new google.maps.LatLngBounds();

         //Create map from google
         map = new google.maps.Map( document.getElementById('map'), {
             mapTypeId: 'satellite'
         });

         //Add a heatmap to the google map
         heatmap = new google.maps.visualization.HeatmapLayer({
             data: getPoints(result['receipts']),
             map: map
         });

         map.fitBounds(bounds); //Scale to view all points
         map.panToBounds(bounds); //Pan to center of all points
     })
     .fail(function() {
         console.error("Failed to gather receipts for heatmap");
     });
    
}

function getPoints(receipts)
{
    var points = [];
    if(receipts != null) {
        $.each(receipts, function(i, item){
            if('Location' in item) {
                var loc = new google.maps.LatLng(item.Location.X, item.Location.Y);
                points[i] = {location: loc, weight: item.Total};
                bounds.extend(loc);
            }
        });
    }
    return points;
}
