
// Based on http://christopher5106.github.io/web/2015/12/13/HTML5-file-image-upload-and-resizing-javascript-with-progress-bar.html
function customXhr(options) {
  return function() {
    var xhr = new window.XMLHttpRequest();
    if (options.uploadProgress && xhr.upload) {
      xhr.upload.addEventListener("progress", options.uploadProgress, false);
    }
    if (options.downloadProgress) {
      xhr.addEventListener("progress", options.downloadProgress, false);
    }
    return xhr;
  };
}

$.readFileToDataURLAsync = function (blob) {
  return $.Deferred(function (op) {
    var reader = new FileReader();
    reader.onload = function (readerEvent) {
      op.resolve(readerEvent.target.result);
    };
    reader.onerror = reader.onabort = function (event) {
      op.reject(event, reader);
    };
    reader.readAsDataURL(blob);
  }).promise();
};

$.loadImageFromUrlAsync = function (imageUrl) {
  return $.Deferred(function (op) {
    var image = new Image();
    image.onload = function(imageEvent) {
      op.resolve(image);
    };
    image.onerror = image.onabort = function (event) {
      op.reject(event, image);
    };
    image.src = imageUrl;
  }).promise();
};

$.geolocateAsync = function(options) {
  return $.Deferred(function(op) {
    if (!navigator || !navigator.geolocation) {
      op.reject(null, "Geolocation not supported by this browser");
      return;
    }
    navigator.geolocation.getCurrentPosition(
      function (position) {
        op.resolve(position.coords, position.timestamp);
      },
      function (err) {
        op.reject(err.code, err.message);
      },
      options
    );
  }).promise();
};

// Take a Promise that might fail and make it into Promise that always
// completes
$.allowFailure = function(promise) {
  return $.Deferred(function(op) {
    promise.always(op.resolve.bind(op));
  }).promise();
};

AsyncChain = function() {
  this.opList = [];
}
AsyncChain.prototype = {
  add: function(fn) { this.opList.push(fn); return this; },
  promiseTo: function(fn) {
    var deferred = $.Deferred();
    this.add(deferred.resolve.bind(deferred));
    deferred.always(this.executeNext.bind(this));
    fn(deferred.promise());
    return this;
  },
  executeNext: function() { (this.opList.shift() || function() {})(this); }
};

$(function () {
  $('button[type="button"][confirm-submit]').click(function() {
    var $this = $(this), prompt = $this.attr('confirm-submit');
    if (window.confirm(prompt)) {
      $this.closest('form').submit();
    }
  })
});
