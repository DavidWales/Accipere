var parameters;
var receiptID;
var editing = false;
var fMapMarker = null;
var map = null;

window.onload = function(){
    //var retrievedStrReceipts = localStorage.getItem('receipts');
    //var receipts = JSON.parse(retrievedStrReceipts);
    //var receipt = receipts.filter(function(element) {
    //    if(element.Id == window.receiptID) {
    //        return element;
    //    }
    //})[0];
    var rData = {
        id: window.receiptID
    };
    $.get({
        url: "/receiptInfo",
        data: rData,
        dataType: "json"
    })
     .done(function(result) {
         loadInfo(result);
         console.log("hit");
     })
     .fail(function() {
         console.error("Failed to fetch receipt info.");
     });
};

function initMap() {
    var locLatLng = new google.maps.LatLng(40.1136604, -88.22595340);
    var myOptions = {
        zoom: 15,
        center: locLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById('receipt_loc'), myOptions);
}

function loadInfo(receipt) {
    if('Location' in receipt) {
        var locLatLng = new google.maps.LatLng(receipt.Location.X, receipt.Location.Y);
        fMapMarker = new google.maps.Marker({
            position: locLatLng,
            map: map
        });
        map.setCenter(locLatLng);
    } else {
        console.warn("No 'Location' key in receipt.");
    }
}

function editLocation(event) {
    if(fMapMarker != null) {
        fMapMarker.setMap(null);
    }

    fMapMarker = new google.maps.Marker({
        position: event.latLng,
        map: map
    });
}

function deleteReceipt() {
  $.ajax({
    method: "DELETE",
    url: window.location
  })
    .done(function(result) {
      window.location.assign("/userhome");
    })
    .fail(function() {
      window.alert("Deletion failed.");
    })
    ;
}

function enableEditing() {
    var inputs = document.querySelectorAll('input, textarea');
    document.getElementById('submitBtn').style.display = null;
    for(i = 0; i < inputs.length; i++) {
        inputs[i].readOnly = false;
        inputs[i].style.border = "1px solid #ccc";
    }

    google.maps.event.addListener(map, 'click', function(event) {
        if(fMapMarker != null) {
            fMapMarker.setMap(null);
        }

        fMapMarker = new google.maps.Marker({
            position: event.latLng,
            map: map
        });
    });
    
    editing = true;
}

function submitEdit() {
    var rdata = {
        total: $('#total').val(),
        notes: $('#notes').val(),
        date: $('#date').val(),
        latitude: fMapMarker.getPosition().lat(),
        longitude: fMapMarker.getPosition().lng(),
        account: $('#account').val(),
        id: window.receiptID
    };
    $.post({
        url: "/receiptEdit",
        data: rdata
    })
     .done(function() {
         var inputs = document.querySelectorAll('input, textarea');
         document.getElementById('submitBtn').style.display = "none";
         for(i = 0; i < inputs.length; i++) {
             inputs[i].readOnly = true;
             inputs[i].style.border = "none";
         }

         google.maps.event.clearListeners(map, 'click');
         
         editing = false;
     })
     .fail(function() {
         console.error("Failed to submit edits.");
     });
}
