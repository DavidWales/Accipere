//Called when user successfully signed into Google.
function onSuccess(googleUser)
{
    var currXhr = new XMLHttpRequest();
    currXhr.onreadystatechange = function()
    {
        if (currXhr.readyState == XMLHttpRequest.DONE)
	{
	    var userProf = googleUser.getBasicProfile();
	    localStorage.setItem('userName', userProf.getName());
	    localStorage.setItem('userEmail', userProf.getEmail());
            window.location.assign(currXhr.responseURL);
        }
    }
    currXhr.open("POST", "/tokenLogin?token=" + googleUser.getAuthResponse().id_token);
    currXhr.send(null);
}

//Called when user failed to sign into Google.
function onFailure(error)
{
    window.location.assign("/?loginFailed=true");
}

//Creates and formats Google sign-in button
function renderButton()
{
    gapi.signin2.render('g-signin', {
	'scope': 'profile email',
	'width': 240,
	'height': 50,
	'longtitle': true,
	'theme': 'dark',
	'onsuccess': onSuccess,
	'onfailure': onFailure
    });
}
