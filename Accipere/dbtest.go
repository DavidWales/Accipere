package main

import (
	"context"
	"flag"
	"os"
	"log"
	
	"github.com/google/subcommands"
	sc "./accipere_dbtest"
	db "./accipere_db"
)

var (
	cxnStrFlag = flag.String("connect", "host=/run/postgresql", "github.com/lib/pq connection string")
)

func main() {
	subcommands.ImportantFlag("connect")
	
	subcommands.Register(subcommands.HelpCommand(), "usage")
	subcommands.Register(subcommands.FlagsCommand(), "usage")
	subcommands.Register(subcommands.CommandsCommand(), "usage")
	sc.RegisterSubcommands()
	
	flag.Parse()
	
	os.Exit(run())
}

func run() int {
	dba, err := db.NewDBAccess(*cxnStrFlag)
	if err != nil {
		log.Print(err)
		return 1
	}
	defer dba.Close()
	sc.Db = dba
	
	ctx := context.Background()
	return int(subcommands.Execute(ctx))
}
