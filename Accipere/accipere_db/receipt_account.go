package accipere_db

import (
	"context"
	"log"
)

func (db *dbAccess) CreateReceiptAccount(ctx context.Context, userId int, name string) (int, error) {
	wrapError := errorWrapper("CreateReceiptAccount")
	stmt, err := db.getStmt(ctx, `
		INSERT INTO account (user_id, name)
		VALUES ($1, $2)
		RETURNING id
		`)
	if err != nil {
		return 0, wrapError("preparing statement", err)
	}
	var accountId int
	err = stmt.QueryRowContext(ctx, userId, name).Scan(&accountId)
	if isUniquenessViolation(err) {
		return 0, ErrAccountAlreadyExists
	} else if err != nil {
		return 0, wrapError("executing insert", err)
	}
	return accountId, nil
}

func (db *dbAccess) GetReceiptAccountList(ctx context.Context, userId int, page *Page) ([]Account, error) {
	wrapError := errorWrapper("GetReceiptAccountList")
	stmt, err := db.getStmt(ctx, `
		SELECT id, name
		FROM account
		WHERE user_id = $1
		ORDER BY name
		LIMIT $2 OFFSET $3
		`)
	if err != nil {
		return nil, wrapError("preparing statement", err)
	}
	limit, offset := limitAndOffset(page)
	rows, err := stmt.QueryContext(ctx, userId, limit, offset)
	if err != nil {
		return nil, wrapError("executing query", err)
	}
	result := makeReceiptAccountListSlice(page)
	var na Account
	for rows.Next() {
		err = rows.Scan(&na.Id, &na.Name)
		if err != nil {
			log.Print(err)
		} else {
			result = append(result, na)
		}
	}
	err = rows.Err()
	if err != nil {
		return nil, wrapError("retrieving rows", err)
	}
	return result, nil
}

func makeReceiptAccountListSlice(page *Page) []Account {
	n := 50
	if page != nil {
		n = page.Count
	}
	return make([]Account, 0, n)
}

func (db *dbAccess) DeleteReceiptAccount(ctx context.Context, userId int, name string) error {
	wrapError := errorWrapper("DeleteReceiptAccount")
	result, err := db.raw_db.ExecContext(
		ctx,
		`
		DELETE FROM account
		WHERE user_id = $1
		AND name = $2
		`,
		userId,
		name,
	)
	if err != nil {
		return wrapError("executing delete", err)
	}
	nRows, err := result.RowsAffected()
	if err != nil {
		log.Print(err)
		return nil
	}
	if nRows == 0 {
		return ErrNoAccount
	}
	return nil
}

func (db *dbAccess) AllowUserToViewAccount(ctx context.Context, userId int, accountName string, allowedUserEmail string) error {
	wrapError := errorWrapper("AllowUserToViewAccount")
	stmt, err := db.getStmt(ctx, `
		INSERT INTO allowed_to_view (account_id, allowed_user_id)
		SELECT a.id, au.id
		FROM account a, "user" au
		WHERE a.user_id = $1
		AND a.name = $2
		AND au.email = $3
		ON CONFLICT DO NOTHING
		`)
	if err != nil {
		return wrapError("preparing statement", nil)
	}
	result, err := stmt.ExecContext(ctx, userId, accountName, allowedUserEmail)
	if err != nil {
		return wrapError("executing statement", nil)
	}
	nInserts, err := result.RowsAffected()
	if err != nil {
		// (shrug) the insert worked but it won't tell me about the result.
		return ErrNoViewingChange
	}
	if nInserts <= 0 {
		return ErrNoViewingChange
	}
	return nil
}

func (db *dbAccess) GetAccountViewers(ctx context.Context, userId int, accountName string, page *Page) ([]string, error) {
	wrapError := errorWrapper("GetAccountViewers")
	stmt, err := db.getStmt(ctx, `
		SELECT au.email
		FROM account a
		JOIN allowed_to_view av
			ON a.id = av.account_id
		JOIN "user" au
			ON av.allowed_user_id = au.id
		WHERE a.user_id = $1
		AND a.name = $2
		ORDER BY au.email
		LIMIT $3 OFFSET $4
		`)
	if err != nil {
		return nil, wrapError("preparing statement", err)
	}
	limit, offset := limitAndOffset(page)
	rows, err := stmt.Query(userId, accountName, limit, offset)
	if err != nil {
		return nil, wrapError("executing query", err)
	}
	result := makeStringSlice(page)
	var email string
	for rows.Next() {
		err = rows.Scan(&email)
		if err != nil {
			log.Print(err)
		} else {
			result = append(result, email)
		}
	}
	err = rows.Err()
	if err != nil {
		return nil, wrapError("retrieving rows", err)
	}
	return result, nil
}

func (db *dbAccess) ForbidUserToViewAccount(ctx context.Context, userId int, accountName string, forbiddenUserEmail string) error {
	wrapError := errorWrapper("ForbidUserToViewAccount")
	stmt, err := db.getStmt(ctx, `
		DELETE FROM allowed_to_view
		WHERE account_id IN (
			SELECT a.id
			FROM account a
			WHERE a.user_id = $1
			AND a.name = $2
		)
		AND allowed_user_id IN (
			SELECT au.id
			FROM "user" au
			WHERE au.email = $3
		)
		`)
	if err != nil {
		return wrapError("preparing statement", err)
	}
	result, err := stmt.ExecContext(ctx, userId, accountName, forbiddenUserEmail)
	if err != nil {
		return wrapError("executing statement", err)
	}
	nRows, err := result.RowsAffected()
	if err != nil {
		// (shrug) the delete worked but it won't tell me about the result.
		return wrapError("inspecting result", err)
	}
	if nRows <= 0 {
		return ErrNoViewingChange
	}
	return nil
}