package accipere_db

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"log"
	"reflect"
	"sort"
	"strings"
	
	geo "../accipere_geo"
)

func (db *dbAccess) SaveReceipt(ctx context.Context, userId int, location geo.Point, image []byte) (int, error) {
	wrapError := errorWrapper("SaveReceipt")
	
	sql := `
		INSERT INTO receipt (user_id, receipt_date, image, location)
		VALUES ($1, CURRENT_DATE, $2, ST_GeographyFromText($3))
		RETURNING id
	`
	vals := []interface{}{userId, image, nil}
	
	if location == nil {
		sql = `
			INSERT INTO receipt (user_id, receipt_date, image)
			VALUES ($1, CURRENT_DATE, $2)
			RETURNING id
		`
		vals = vals[:2]
	} else {
		vals[2] = location.PointGeometryString()
	}
	
	stmt, err := db.getStmt(ctx, sql)
	if err != nil {
		return 0, wrapError("preparing statement", err)
	}
	
	var receiptId int
	err = stmt.QueryRowContext(ctx, vals...).Scan(&receiptId)
	if err != nil {
		return 0, wrapError("executing statement", err)
	}
	
	return receiptId, nil
}

func (db *dbAccess) GetReceiptList(ctx context.Context, userId int, page *Page) ([]Receipt, error) {
	wrapError := errorWrapper("GetReceiptList")
	stmt, err := db.getStmt(ctx, `
		SELECT r.id
			, r.user_id
			, u.name
			, u.email
			, r.account_id
			, a.name as account
			, r.receipt_date
			, ST_AsText(r.location)
			, r.total::DECIMAL as total
			, r.notes
		FROM receipt r
		JOIN "user" u
			ON r.user_id = u.id
		LEFT JOIN account a
			ON r.account_id = a.id
		WHERE r.user_id = $1
		OR r.account_id IN (
			SELECT av.account_id
			FROM allowed_to_view av
			WHERE av.allowed_user_id = $1
		)
		ORDER BY receipt_date DESC, id DESC
		LIMIT $2 OFFSET $3
		`)
	if err != nil {
		return nil, wrapError("preparing statement", err)
	}
	limit, offset := limitAndOffset(page)
	rows, err := stmt.Query(userId, limit, offset)
	if err != nil {
		return nil, wrapError("executing query", err)
	}
	result := makeReceiptListSlice(page)
	var nr Receipt
	for rows.Next() {
		err = nr.readInfo(rows, nil)
		if err != nil {
			log.Print(err)
		} else {
			result = append(result, nr)
		}
	}
	err = rows.Err()
	if err != nil {
		return nil, wrapError("retrieving rows", err)
	}
	return result, nil
}

func (db *dbAccess) GetCustomizedReceiptList(ctx context.Context, userId int, cust *QueryCustomization, page *Page) ([]Receipt, error) {
	if cust == nil {
		return db.GetReceiptList(ctx, userId, page)
	}
	wrapError := errorWrapper("GetReceiptList")
	var custArgs []interface{}
	sql := new(bytes.Buffer)
	sql.WriteString(`
		SELECT r.id
			, r.user_id
			, u.name
			, u.email
			, r.account_id
			, a.name as account
			, r.receipt_date
			, ST_AsText(r.location)
			, r.total::DECIMAL as total
			, r.notes
		`)
	for _, erc := range cust.ExtraResultColumns {
		colSql, colArgs := renderSqlClause(erc.Expression, 4 + len(custArgs))
		sql.WriteString(", ")
		sql.WriteString(colSql)
		custArgs = append(custArgs, colArgs...)
	}
	sql.WriteString(`
		FROM receipt r
		JOIN "user" u
			ON r.user_id = u.id
		LEFT JOIN account a
			ON r.account_id = a.id
		WHERE (r.user_id = $1
		OR r.account_id IN (
			SELECT av.account_id
			FROM allowed_to_view av
			WHERE av.allowed_user_id = $1
		)) `)
	if cust.Filter != nil {
		sql.WriteString("AND ")
		filtSql, filtArgs := renderSqlClause(cust.Filter, 4 + len(custArgs))
		sql.WriteString(filtSql)
		custArgs = append(custArgs, filtArgs...)
	}
	sql.WriteString(" ORDER BY ")
	if cust.Ordering != nil {
		ordSql, ordArgs := renderSqlClause(cust.Ordering, 4 + len(custArgs))
		sql.WriteString(ordSql)
		custArgs = append(custArgs, ordArgs...)
	} else {
		sql.WriteString("receipt_date DESC, id DESC")
	}
	sql.WriteString(" LIMIT $2 OFFSET $3")
	stmt, err := db.getStmt(ctx, sql.String())
	if err != nil {
		return nil, wrapError("preparing statement", err)
	}
	limit, offset := limitAndOffset(page)
	queryArgs := make([]interface{}, 0, 3 + len(custArgs))
	queryArgs = append(queryArgs, userId, limit, offset)
	queryArgs = append(queryArgs, custArgs...)
	rows, err := stmt.Query(queryArgs...)
	if err != nil {
		return nil, wrapError("executing query", err)
	}
	result := makeReceiptListSlice(page)
	var nr Receipt
	cfs := newCustomFieldScanner(cust)
	err = cfs.prepareCapture(rows)
	if err != nil {
		return nil, wrapError("preparing extra result column capture", err)
	}
	for rows.Next() {
		err = nr.readInfo(rows, cfs)
		if err != nil {
			log.Print(err)
		} else {
			result = append(result, nr)
		}
		cfs.outputCustomFields()
	}
	err = rows.Err()
	if err != nil {
		return nil, wrapError("retrieving rows", err)
	}
	return result, nil
}

func makeReceiptListSlice(page *Page) []Receipt {
	n := 50
	if page != nil {
		n = page.Count
	}
	return make([]Receipt, 0, n)
}

func renderSqlClause(o SqlClause, nFirstArg int) (string, []interface{}) {
	clause := new(bytes.Buffer)
	argVals := make([]interface{}, 0, 1)
	nArg := nFirstArg
	for _, term := range o.Parts() {
		switch term := term.(type) {
		case string:
			clause.WriteString(term)
		case QueryVariable:
			clause.WriteString(fmt.Sprintf("$%v", nArg))
			nArg++
			argVals = append(argVals, term.Value)
		default:
			panic(fmt.Errorf("%T invalid as Ordering part", term))
		}
	}
	return clause.String(), argVals
}

type customFieldScanner struct {
	cust *QueryCustomization
	rowCaptureSlots []interface{}
	startField int
}

func newCustomFieldScanner(cust *QueryCustomization) *customFieldScanner {
	result := new(customFieldScanner)
	result.cust = cust
	return result
}

func (cfs *customFieldScanner) prepareCapture(rows *sql.Rows) error {
	colTypes, err := rows.ColumnTypes()
	if err != nil {
		return err
	}
	nFields := len(colTypes)
	if len(cfs.rowCaptureSlots) < nFields {
		cfs.rowCaptureSlots = make([]interface{}, nFields)
	}
	for i := range colTypes {
		cfs.rowCaptureSlots[i] = reflect.New(colTypes[i].ScanType()).Interface()
	}
	for _, erc := range cfs.cust.ExtraResultColumns {
		erc.OutputValues = nil
	}
	return nil
}

func (cfs *customFieldScanner) outputCustomFields()  {
	for i, erc := range cfs.cust.ExtraResultColumns {
		capVal := reflect.ValueOf(cfs.rowCaptureSlots[cfs.startField + i])
		erc.OutputValues = append(erc.OutputValues, capVal.Elem().Interface())
		cfs.cust.ExtraResultColumns[i] = erc
	}
}

func (r *Receipt) readInfo(row rowScanner, cfs *customFieldScanner) error {
	var accountName sql.NullString
	fields := []interface{} {
		&r.Id,
		&r.OwnerId, &r.OwnerName, &r.OwnerEmail,
		&r.Account_id, &accountName,
		&r.Date, &r.Location, &r.Total, &r.Notes,
	}
	if cfs != nil {
		// Append capture slots for all the custom fields
		cfs.startField = len(fields)
		fields = append(fields, cfs.rowCaptureSlots[len(fields):]...)
	}
	err := row.Scan(fields...)
	if err != nil {
		return err
	}
	if accountName.Valid {
		r.Account = accountName.String
	} else {
		// Empty string is not accepted as a valid account name
		r.Account = ""
	}
	return nil
}

func (db *dbAccess) GetReceiptInfo(ctx context.Context, userId int, receiptId int) (*Receipt, error) {
	wrapError := errorWrapper("GetReceiptInfo")
	stmt, err := db.getStmt(ctx, `
		SELECT r.id
			, r.user_id
			, u.name
			, u.email
			, r.account_id
			, a.name as account
			, r.receipt_date
			, ST_AsText(r.location)
			, r.total::DECIMAL as total
			, r.notes
		FROM receipt r
		JOIN "user" u
			ON r.user_id = u.id
		LEFT JOIN account a
			ON r.account_id = a.id
		WHERE r.id = $2
		AND (
			r.user_id = $1
			OR
			r.account_id IN (
				SELECT av.account_id
				FROM allowed_to_view av
				WHERE av.allowed_user_id = $1
			)
		)
		`)
	if err != nil {
		return nil, wrapError("preparing query", err)
	}
	var result Receipt
	err = result.readInfo(stmt.QueryRowContext(ctx, userId, receiptId), nil)
	if err == sql.ErrNoRows {
		return nil, ErrNoReceipt
	} else if err != nil {
		return nil, wrapError("executing query", err)
	}
	return &result, nil
}

func (db *dbAccess) GetReceiptImage(ctx context.Context, userId int, receiptId int) ([]byte, error) {
	wrapError := errorWrapper("GetReceiptImage")
	stmt, err := db.getStmt(ctx, `
		SELECT r.image
		FROM receipt r
		WHERE r.id = $2
		AND (
			r.user_id = $1
			OR
			r.account_id IN (
				SELECT av.account_id
				FROM allowed_to_view av
				WHERE av.allowed_user_id = $1
			)
		)
		`)
	if err != nil {
		return nil, wrapError("preparing statement", err)
	}
	var result []byte
	err = stmt.QueryRowContext(ctx, userId, receiptId).Scan(&result)
	if err == sql.ErrNoRows {
		return nil, ErrNoReceipt
	} else if err != nil {
		return nil, wrapError("executing query", err)
	}
	return result, nil
}

func (db *dbAccess) SetReceiptFields(ctx context.Context, userId int, receiptId int, fieldAssignments ...ReceiptFieldAssignment) error {
	wrapError := errorWrapper("SetReceiptFields")
	
	if len(fieldAssignments) == 0 {
		return nil
	}
	
	// Sort the fields by field name so we don't have a bunch of variations on
	// the same query, just with the assignments in different orders, clogging
	// up the query cache on the database:
	sort.Slice(fieldAssignments, func(i, j int) bool {
		return fieldAssignments[i].FieldName < fieldAssignments[j].FieldName
	})
	
	assgnList, values := rfAssignmentParts(fieldAssignments, userId, receiptId)
	
	stmt, err := db.getStmt(
		ctx,
		"UPDATE receipt\n" +
		"SET " + assgnList + "\n" +
		"WHERE user_id = $1\n" +
		"AND id = $2",
	)
	if err != nil {
		return wrapError("preparing statment", err)
	}
	result, err := stmt.ExecContext(ctx, values...)
	if err != nil {
		return wrapError("executing update", err)
	}
	nRows, err := result.RowsAffected()
	if err != nil {
		log.Print(err)
		return nil
	}
	if nRows == 0 {
		return ErrNoReceipt
	}
	return nil
}

func rfAssignmentParts(items []ReceiptFieldAssignment, leadingValues ...interface{}) (string, []interface{}) {
	parts := make([]string, len(items))
	values := make([]interface{}, len(leadingValues) + len(items))
	for i, v := range leadingValues {
		values[i] = v
	}
	for i, a := range items {
		switch v := a.Value.(type) {
		case geo.Point:
			parts[i] = fmt.Sprintf("%s = ST_GeographyFromText($%v)", string(a.FieldName), i + len(leadingValues) + 1)
			values[len(leadingValues) + i] = v.PointGeometryString()
		default:
			parts[i] = fmt.Sprintf("%s = $%v", string(a.FieldName), i + len(leadingValues) + 1)
			values[len(leadingValues) + i] = a.Value
		}
	}
	return strings.Join(parts, ", "), values
}

func (db *dbAccess) SetReceiptAccount(ctx context.Context, userId int, receiptId int, accountName string) (string, error) {
	wrapError := errorWrapper("SetReceiptAccount")
	stmt, err := db.getStmt(ctx, `
		UPDATE receipt
		SET account_id = (
			SELECT a.id
			FROM account a
			WHERE a.user_id = $1
			AND a.name = $3
		)
		WHERE user_id = $1
		AND id = $2
		RETURNING (
			SELECT a.name
			FROM account a
			WHERE a.id = account_id
		)
		`)
	if err != nil {
		return "", wrapError("preparing statement", err)
	}
	var dbResult sql.NullString
	err = stmt.QueryRowContext(ctx, userId, receiptId, accountName).Scan(&dbResult)
	if err == sql.ErrNoRows {
		return "", ErrNoReceipt
	}
	if err != nil {
		return "", wrapError("executing statement", err)
	}
	if !dbResult.Valid {
		return "", nil
	}
	return dbResult.String, nil
}

func (db *dbAccess) DeleteReceipt(ctx context.Context, userId int, receiptId int) error {
	wrapError := errorWrapper("DeleteReceipt")
	result, err := db.raw_db.ExecContext(
		ctx,
		`
		DELETE FROM receipt
		WHERE user_id = $1
		AND id = $2
		`,
		userId,
		receiptId,
	)
	if err != nil {
		return wrapError("executing command", err)
	}
	nRows, err := result.RowsAffected()
	if err != nil {
		log.Print(err)
		return nil
	}
	if nRows == 0 {
		return ErrNoReceipt
	}
	return nil
}
