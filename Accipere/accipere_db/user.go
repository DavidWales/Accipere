package accipere_db

import (
	"context"
	"log"
	
	"golang.org/x/crypto/bcrypt"
)

func (db *dbAccess) lookupUserInfo(ctx context.Context, email string) (*userInfo, error){
	wrapError := errorWrapper("lookupUserInfo")
	
	stmt, err := db.getStmt(ctx, `
		SELECT id, passhash
		FROM "user"
		WHERE email = $1
		`)
	if err != nil {
		return nil, wrapError("preparing statement", err)
	}
	
	rows, err := stmt.QueryContext(ctx, email)
	if err != nil {
		return nil, wrapError("executing query", err)
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, nil
	}
	var result userInfo
	err = rows.Scan(&result.id, &result.passhash)
	if err != nil {
		return nil, wrapError("reading result row", err)
	}
	if err = rows.Err(); err != nil {
		return nil, wrapError("final check on rows", err)
	}
	return &result, nil
}

type userInfo struct {
	id int
	passhash []byte
}

func (ui *userInfo) validate(cred string) bool {
	if ui == nil {
		return false
	}
	
	err := bcrypt.CompareHashAndPassword(ui.passhash, []byte(cred))
	if err != nil {
		log.Printf("error: %v\n", err)
		return false
	}
	
	return true
}
