package accipere_db

import (
	geo "../accipere_geo"
)

// This type should implement the SqlClause interface.
type OrderByStoredData struct {
	// This may be any SQL expression formed only using constants and values
	// accessible in the query.  E.g.:
	//     ExprOfTuple = string(RFDate)
	ExprOfTuple string
	
	Descending bool
}

func (o *OrderByStoredData) Parts() []interface{}  {
	dir := " ASC"
	if o.Descending {
		dir = " DESC"
	}
	return []interface{} {o.ExprOfTuple, dir}
}

// This type should implement the SqlClause interface.
type OrderByDistance struct {
	// This may be any SQL expression resulting in a geographic value,
	// typically from a field accessible in the query, e.g.:
	//     ExprOfTuple = string(RFLocation)
	ExprOfTuple string
	
	// The actual point being queried
	QueryPoint geo.Point
}

func (o *OrderByDistance) Parts() []interface{} {
	return []interface{} {
		o.ExprOfTuple,
		" <-> ST_GeographyFromText(",
		QueryVariable{o.QueryPoint.PointGeometryString()},
		")",
	}
}

// This type should implement the SqlClause interface.  Use it to combine
// other ordering clauses into a hierarchy.
type OrderingHierarchy struct {
	Terms []SqlClause
}

func (o *OrderingHierarchy) Parts() []interface{} {
	return joinParts(o.Terms, ", ")
}

// This type should implement the SqlClause interface.
type FilterBySingleValue struct {
	// This may be any SQL expression formed only using constants and values
	// accessible in the query.  E.g.:
	//     ExprOfTuple = string(RFDate)
	ExprOfTuple string
	
	// This gives the value against which ExprOfTuple must match in a returned
	// record.
	TargetValue interface{}
}

func (f *FilterBySingleValue) Parts() []interface{} {
	return []interface{} {
		f.ExprOfTuple,
		" = ",
		QueryVariable{f.TargetValue},
	}
}

// This type should implement the SqlClause interface.  It is allowable to
// leave RangeStart and/or RangeEnd as nil to indicate unboundedness.
type FilterByRange struct {
	// This may be any SQL expression formed only using constants and values
	// accessible in the query.  E.g.:
	//     ExprOfTuple = string(RFDate)
	ExprOfTuple string
	
	// The value of the beginning of the selected range; may be nil for
	// unbounded.
	RangeStart interface{}
	
	// Indicates if the range starting value is excluded from the range.
	RangeStartOpen bool
	
	// The value of the ending of the selected range; may be nil for unbounded.
	RangeEnd interface{}
	
	// Indicates if the range ending value is excluded from the range.
	RangeEndOpen bool
}

func (f *FilterByRange) Parts() []interface{} {
	rngStartOp := " <= "
	if f.RangeStartOpen { rngStartOp = " < " }
	rngEndOp := " <= "
	if f.RangeEndOpen { rngEndOp = " < " }
	
	switch {
	case f.RangeStart == nil && f.RangeEnd == nil:
		return []interface{} {"TRUE"}
	case f.RangeEnd == nil:
		return []interface{} {
			QueryVariable{f.RangeStart},
			rngStartOp,
			f.ExprOfTuple,
		}
	case f.RangeStart == nil:
		return []interface{} {
			f.ExprOfTuple,
			rngEndOp,
			QueryVariable{f.RangeEnd},
		}
	}
	return []interface{} {
		QueryVariable{f.RangeStart},
		rngStartOp,
		f.ExprOfTuple,
		" AND ",
		f.ExprOfTuple,
		rngEndOp,
		QueryVariable{f.RangeEnd},
	}
}

// This type should implement the SqlClause interface.  Distance is given in
// meters.
type FilterByDistance struct {
	// This may be any SQL expression resulting in a geographic value,
	// typically from a field accessible in the query, e.g.:
	//     ExprOfTuple = string(RFLocation)
	ExprOfTuple string
	
	// This is the reference point to which the distance is measured.
	QueryPoint geo.Point
	
	// This is the maximum allowed distance between the points identified by
	// ExprOfTuple and QueryPoint for rows returned by the query.
	MaxDistance float64
}

func (f *FilterByDistance) Parts() []interface{} {
	return []interface{} {
		"ST_DWithin(",
		f.ExprOfTuple,
		", ST_GeographyFromText(",
		QueryVariable{f.QueryPoint.PointGeometryString()},
		"),",
		QueryVariable{f.MaxDistance},
		")",
	}
}

// This type should implement the SqlClause interface.  Use it to filter for
// the intersection of other Filter... instances.
type FilterIntersection struct {
	Terms []SqlClause
}

func (f *FilterIntersection) Parts() []interface{} {
	if len(f.Terms) == 0 {
		return []interface{}{"TRUE"}
	}
	return joinParts(f.Terms, " AND ")
}

// This type should implement the SqlClause interface.  Use it as the
// Expression in a ResultColumn, and the values stored in the corresponding
// OutputValues will be the distance in meters from QueryPoint to the
// point determined from the record via ExprOfTuple.
type DistanceFromPoint struct {
	// This may be any SQL expression resulting in a geographic value,
	// typically from a field accessible in the query, e.g.:
	//     ExprOfTuple = string(RFLocation)
	ExprOfTuple string
	
	// This is the reference point to which the distance is measured.
	QueryPoint geo.Point
}

func (erc *DistanceFromPoint) Parts() []interface{} {
	return []interface{} {
		"ST_Distance(",
		erc.ExprOfTuple,
		", ST_GeographyFromText(",
		QueryVariable{erc.QueryPoint.PointGeometryString()},
		"))",
	}
}

func joinParts(clauses []SqlClause, joiner string) []interface{} {
	itmdt := make([][]interface{}, len(clauses))
	partCount := 0
	for i, term := range clauses {
		itmdt[i] = term.Parts()
		partCount += len(itmdt[i])
	}
	
	result := make([]interface{}, 0, partCount + len(itmdt))
	for _, parts := range itmdt {
		result = append(result, joiner)
		result = append(result, parts...)
	}
	return result[1:]
}

// Test interface Implementation
var (
	testOrderByStoredData   SqlClause = &OrderByStoredData{}
	testOrderByDistance     SqlClause = &OrderByDistance{}
	testOrderingHierarchy   SqlClause = &OrderingHierarchy{}
	testFilterBySingleValue SqlClause = &FilterBySingleValue{}
	testFilterByRange       SqlClause = &FilterByRange{}
	testFilterByDistance    SqlClause = &FilterByDistance{}
	testFilterIntersection  SqlClause = &FilterIntersection{}
	testDistanceFromPoint   SqlClause = &DistanceFromPoint{}
)
