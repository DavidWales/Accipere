package accipere_db

import (
	"context"

	"golang.org/x/crypto/bcrypt"
)

func (db *dbAccess) CreateLocalAccount(ctx context.Context, email string, name string, passwd string) (int, error) {
	wrapError := errorWrapper("CreateLocalAccount")
	passhash, err := bcrypt.GenerateFromPassword([]byte(passwd), bcrypt.DefaultCost)
	if err != nil {
		return 0, wrapError("hashing password", err)
	}
	
	var userId int
	err = db.raw_db.QueryRowContext(
		ctx,
		`
		INSERT INTO "user" (email, name, passhash)
		VALUES ($1, $2, $3)
		RETURNING id
		`,
		email,
		name,
		passhash,
	).Scan(&userId)
	if err != nil {
		return 0, wrapError("inserting user record", err)
	}
	return userId, nil
}
