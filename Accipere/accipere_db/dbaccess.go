package accipere_db

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"strconv"
	"sync"
	"time"
	
	"github.com/shopspring/decimal"
	auth "../accipere_auth"
	geo "../accipere_geo"
	
	_ "github.com/lib/pq"
)

type DBAccess interface {
	Close()
	
	TokenValidityMinutes() int
	
	SetTokenValidityMinutes(int)
	
	CreateLocalAccount(ctx context.Context, email string, name string, passwd string) (int, error)
	
	// Returns a session token unless err != nil
	CreateLoginSession(ctx context.Context, email string, proof auth.BonaFides) ([]byte, error)
	
	// Returns user ID unless err != nil
	AccessLoginSession(ctx context.Context, token []byte) (int, error)
	
	AccessLoginSessionUser(ctx context.Context, token []byte) (*User, error)
	
	SaveSessionState(ctx context.Context, user *User) error
	
	TerminateLoginSession(ctx context.Context, token []byte) error
	
	TerminateLoginSessionUser(ctx context.Context, user *User) error
	
	// Save a receipt for a user taken at a location and return the receipt's
	// ID unless err != nil
	SaveReceipt(ctx context.Context, userId int, location geo.Point, image []byte) (int, error)
	
	// Passing nil for page is acceptable and will retrieve the whole list
	GetReceiptList(ctx context.Context, userId int, page *Page) ([]Receipt, error)
	
	// Passing nil for page is acceptable and will retrieve the whole list.
	// Passing nil for cust is acceptable and is the same as calling
	// GetReceiptList with the remaining arguments.
	GetCustomizedReceiptList(ctx context.Context, userId int, cust *QueryCustomization, page *Page) ([]Receipt, error)
	
	GetReceiptInfo(ctx context.Context, userId int, receiptId int) (*Receipt, error)
	
	GetReceiptImage(ctx context.Context, userId int, receiptId int) ([]byte, error)
	
	// Call this method like:
	//     err = db.SetReceiptFields(ctx, userId, receiptId,
	//         ReceiptFieldAssignment{RFTotal, totalAmount},
	//         ReceiptFieldAssignment{RFNotes, newNotes},
	//     )
	SetReceiptFields(ctx context.Context, userId int, receiptId int, fieldAssignments ...ReceiptFieldAssignment) error
	
	// Returns the actual name of the account to which the identified receipt
	// has been assigned -- which can be an empty string to indicate no
	// assigned account.  Pass in a string as accountName that does not match
	// an existing account name to remove the receipt from all accounts.  If
	// the identified receipt doesn't belong to the identified user or if
	// no such receipt exists, ErrNoReceipt will be returned as error.
	SetReceiptAccount(ctx context.Context, userId int, receiptId int, accountName string) (string, error)
	
	DeleteReceipt(ctx context.Context, userId int, receiptId int) error
	
	// Returns the ID of the new account or an error, which may be
	// ErrAccountAlreadyExists.
	CreateReceiptAccount(ctx context.Context, userId int, name string) (int, error)
	
	// Passing nil for page is acceptable and will retrieve the whole list
	GetReceiptAccountList(ctx context.Context, userId int, page *Page) ([]Account, error)
	
	DeleteReceiptAccount(ctx context.Context, userId int, name string) error
	
	// May return ErrNoViewingChange
	AllowUserToViewAccount(ctx context.Context, userId int, accountName string, allowedUserEmail string) error
	
	GetAccountViewers(ctx context.Context, userId int, accountName string, page *Page) ([]string, error)
	
	ForbidUserToViewAccount(ctx context.Context, userId int, accountName string, forbiddenUserEmail string) error
}

type User struct {
	Id int
	Name string
	Email string
	sessionToken []byte
	sessionState json.RawMessage
}

type PairwiseSerializable interface {
	PairStream() <-chan struct{ Key string; Value interface{} }
}

// This type is used for paginating database results.
type Page struct {
	Start int
	Count int
}

type Receipt struct {
	Id int
	OwnerId int
	OwnerName string
	OwnerEmail string
	Account_id sql.NullInt64
	Account string
	Date time.Time
	Location geo.NullLongLat
	Total decimal.NullDecimal
	Notes string
}

type Account struct {
	Id int
	Name string
}

type ReceiptFieldName string

// Receipt fields
const (
	RFImage         ReceiptFieldName = "image"
	RFAccountId     ReceiptFieldName = "account_id"
	RFDate          ReceiptFieldName = "receipt_date"
	RFLocation      ReceiptFieldName = "location"
	RFTotal         ReceiptFieldName = "total"
	RFNotes         ReceiptFieldName = "notes"
)

func (fname ReceiptFieldName) Gets(value interface{}) ReceiptFieldAssignment {
	return ReceiptFieldAssignment{
		FieldName: fname,
		Value: value,
	}
}

type ReceiptFieldAssignment struct {
	FieldName ReceiptFieldName
    Value interface{}
}

type QueryCustomization struct {
	ExtraResultColumns []ResultColumn
	Filter SqlClause
	Ordering SqlClause
}

type ResultColumn struct {
	Expression SqlClause
	
	// This field will be used to capture the output values generated.  The
	// indexes into this slice correspond to the indexes into the slice of
	// the primary record type returned by the DBAccess method.
	OutputValues []interface{}
}

func ResultColumns(exprs ...SqlClause) []ResultColumn {
	result := make([]ResultColumn, len(exprs))
	for i := range exprs {
		result[i].Expression = exprs[i]
	}
	return result
}

func (cust *QueryCustomization) GetExtraResult(row, col int) interface{} {
	return cust.ExtraResultColumns[col].OutputValues[row]
}

type SqlClause interface {
	// The items of the returned slice must be either string or
	// QueryVariable
	Parts() []interface{}
}

type QueryVariable struct {
	Value interface{}
}

// Create a new DBAccess from a github.com/lib/pq connection parameters
// string (https://godoc.org/github.com/lib/pq#hdr-Connection_String_Parameters)
//
// On Ubuntu Xenial, connecting via Unix domain socket to the local server
// can use the string "host=/run/postgresql", which supports local account
// authentication (no password).
//
// It is idiomatic to
//   defer db.Close()
// on the returned DBAccess when the access is limited to the scope of the
// calling function, and is ideal to use near the top level of a program (such
// as a Web server).  The DBAccess object is intended to use an sql.DB object,
// and is intended to be similarly long-lived (http://go-database-sql.org/accessing.html).
func NewDBAccess(param_str string) (DBAccess, error) {
	db, err := sql.Open("postgres", param_str)
	if err != nil {
		return nil, err
	}
	
	return &dbAccess{
		raw_db: db,
		stmts: make(map[string]*sql.Stmt),
		tokenValidityMinutes: 2 * 24 * 60,
		}, nil
}

var (
	ErrNoReceipt = errors.New("receipt does not exist")
	ErrAccountAlreadyExists = errors.New("account already exists")
	ErrNoAccount = errors.New("account does not exist")
	ErrNoViewingChange = errors.New("viewability of account did not change")
)

type dbAccess struct {
	raw_db *sql.DB
	tokenValidityMinutes int
	
	mu sync.Mutex
	stmts map[string]*sql.Stmt
}

func (db *dbAccess) Close() {
	db.raw_db.Close()
}

func (db *dbAccess) TokenValidity() string {
	return strconv.Itoa(db.tokenValidityMinutes) + " minutes"
}

func (db *dbAccess) TokenValidityMinutes() int {
	return db.tokenValidityMinutes
}

func (db *dbAccess) SetTokenValidityMinutes(limit int)  {
	db.tokenValidityMinutes = limit
}
