package accipere_db

import (
	"context"
	"database/sql"
	"fmt"
	
	"github.com/lib/pq"
)

type rowScanner interface {
	Scan(...interface{}) error
}

func (db *dbAccess) getStmt(ctx context.Context, stmt_str string) (*sql.Stmt, error) {
	wrapError := errorWrapper("getStmt")
	stmt := db.tryGetStmt(stmt_str)
	if stmt != nil {
		return stmt, nil
	}
	stmt, err := db.raw_db.PrepareContext(ctx, stmt_str)
	if err != nil {
		return nil, wrapError("sql.DB.Prepare", err)
	}
	// db.stmts is not concurency-safe, so lock db.mu while poking around
	db.mu.Lock()
	defer db.mu.Unlock()
	db.stmts[stmt_str] = stmt
	return stmt, nil
}

func (db *dbAccess) tryGetStmt(stmt_str string) *sql.Stmt {
	// db.stmts is not concurency-safe, so lock db.mu while poking around
	db.mu.Lock()
	result := db.stmts[stmt_str]
	db.mu.Unlock()
	return result
}

func errorWrapper(funcName string) func(string, error) error {
	return func(step string, err error) error {
		return fmt.Errorf("%v - %v: %v", funcName, step, err)
	}
}

func limitAndOffset(page *Page) (sql.NullInt64, int) {
	if page == nil {
		return sql.NullInt64{}, 0
	}
	return sql.NullInt64{int64(page.Count), true}, page.Start
}

func isUniquenessViolation(err error) bool {
	pq_err, ok := err.(pq.Error)
	if !ok {
		return false
	}
	return pq_err.Code == "23505"
}

func makeStringSlice(page *Page) []string {
	n := 50
	if page != nil {
		n = page.Count
	}
	return make([]string, 0, n)
}
