package accipere_db

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	
	logr "github.com/Sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	auth "../accipere_auth"
)

func (db *dbAccess) CreateLoginSession(ctx context.Context, email string, proof auth.BonaFides) ([]byte, error) {
	wrapError := errorWrapper("CreateLoginSession")
	userId, err := db.getUserId(ctx, email, proof)
	if err != nil {
		return nil, wrapError("getting user ID", err)
	}
	
	stmt, err := db.getStmt(ctx, `
		INSERT INTO login_session (session_token, user_id, start_timepoint, last_access)
		VALUES ($1, $2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
		`)
	if err != nil {
		return nil, wrapError("preparing insert statement", err)
	}
	
	token := make([]byte, 64)
	n, err := rand.Reader.Read(token)
	if n < len(token) {
		return nil, fmt.Errorf("unable to allocate token")
	}
	
	_, err = stmt.ExecContext(ctx, token, userId)
	if err != nil {
		return nil, wrapError("executing insert", err)
	}
	return token, nil
}

func (db *dbAccess) getUserId(ctx context.Context, email string, proof auth.BonaFides) (int, error) {
	// Cases
	// 1) Known user, proof is password
	// 2) Known, externally identified user, proof is verified ID (i.e. 'sub' for Google)
	// 3) Unknown, externally identified user, proof is verified ID (i.e. 'sub' for Google)
	
	// Look up the user record by email
	ui, err := db.lookupUserInfo(ctx, email)
	if err != nil {
		return 0, err
	}
	
	if proof.RequiresValidation() {
		// Compare proof.PlaintextCredential() to the stored passhash,
		// return error if no match
		if ui.validate(proof.PlaintextCredential()) {
			return ui.id, nil
		} else {
			return 0, &auth.UnauthenticatedUser{email}
		}
	} else if ui != nil {
		return ui.id, nil
	}
	
	// A new user record for an externally identified user is required
	passhash, err := bcrypt.GenerateFromPassword(
		[]byte(proof.PlaintextCredential()),
		bcrypt.DefaultCost,
	)
	if err != nil {
		return 0, err
	}
	var userId int
	err = db.raw_db.QueryRowContext(
		ctx,
		`
		INSERT INTO "user" (email, name, passhash)
		VALUES ($1, $2, $3)
		RETURNING id
		`,
		email,
		proof.Name(),
		passhash,
	).Scan(&userId)
	if err != nil {
		return 0, err
	}
	return userId, nil
}

func (db *dbAccess) AccessLoginSession(ctx context.Context, session_token []byte) (int, error) {
	wrapError := errorWrapper("AccessLoginSession")
	stmt, err := db.getStmt(ctx, `
		UPDATE login_session
		SET last_access = CURRENT_TIMESTAMP
		WHERE session_token = $1
		AND CURRENT_TIMESTAMP <= last_access + $2::INTERVAL
		RETURNING user_id
		`)
	if err != nil {
		return 0, wrapError("preparing statement", err)
	}
	var user_id int
	err = stmt.QueryRowContext(ctx, session_token, db.TokenValidity()).Scan(&user_id)
	if err == sql.ErrNoRows {
		return 0, auth.ErrInvalidSession
	} else if err != nil {
		return 0, wrapError("executing query", err)
	}
	return user_id, nil
}

func (db *dbAccess) AccessLoginSessionUser(ctx context.Context, session_token []byte) (*User, error) {
	wrapError := errorWrapper("AccessLoginSessionUser")
	stmt, err := db.getStmt(ctx, `
		WITH accessed_user AS (
			UPDATE login_session
			SET last_access = CURRENT_TIMESTAMP
			WHERE session_token = $1
			AND CURRENT_TIMESTAMP <= last_access + $2::INTERVAL
			RETURNING user_id, session_data
		)
		SELECT u.id, u.name, u.email, au.session_data
		FROM "user" u
		JOIN accessed_user au
			ON u.id = au.user_id
		`)
	if err != nil {
		return nil, wrapError("preparing statement", nil)
	}
	result := new(User)
	err = stmt.QueryRowContext(ctx, session_token, db.TokenValidity()).Scan(
		&result.Id,
		&result.Name,
		&result.Email,
		&result.sessionState,
	)
	logr.WithField("loaded value", string(result.sessionState)).Debug("Session state loaded")
	if err == sql.ErrNoRows {
		return nil, auth.ErrInvalidSession
	} else if err != nil {
		return nil, wrapError("executing query", err)
	}
	result.sessionToken = make([]byte, len(session_token))
	copy(result.sessionToken, session_token)
	return result, nil
}

func (db *dbAccess) TerminateLoginSession(ctx context.Context, token []byte) error {
	wrapError := errorWrapper("TerminateLoginSession")
	
	sr, err := db.raw_db.ExecContext(
		ctx,
		`
		DELETE FROM login_session
		WHERE token = $1
		`,
		token,
	)
	if err != nil {
		log.Printf("ERROR: Could not terminate session starting %x...\n", token[:8])
		return wrapError("deleting session", err)
	}
	nRows, err := sr.RowsAffected()
	if err != nil {
		// What to do?  Log it, I guess:
		log.Print(err)
	} else if nRows == 0 {
		return auth.ErrInvalidSession
	}
	return nil
}

func (db *dbAccess) TerminateLoginSessionUser(ctx context.Context, user *User) error {
	return db.TerminateLoginSession(ctx, user.sessionToken)
}

// The JSON session data will be unmarshalled into *out*, using whatever mapping
// *out* defines for unmarshalling.  Full control is available by implementing
// the json.Unmarshaller interface.  Please see writeSessionState for notes
// about how writing session state works -- specifically that it is unnecessary
// to read everything just to preserve stored data not being manipulated.
//
// The top level value in the session state is expected to be an object, so
// passing a *string, *int, *float64, or a slice is not recommended.
func (u *User) ReadSessionState(out interface{}) error {
	return json.Unmarshal(u.sessionState, out)
}

// The value given by *data* is converted to a map (as if by marshalling to
// JSON, then unmarshalling to map[string]interface{}) whose entries are
// then used to update the session state stored in the target object.  In
// particular, any JSON object property at the top level not marshalled by
// *data* will be left unchanged.
//
// The method is optimized for *data* as a map[string]interface{} or an
// implementation of PairwiseSerializable.
func (u *User) WriteSessionState(data interface{}) (err error)  {
	var doUpdate func(map[string]interface{})
	
	switch data := data.(type) {
	case map[string]interface{}:
		doUpdate = func(existing map[string]interface{}) {
			logr.Debug("Updating session with map[string]interface{}")
			for k, v := range data {
				existing[k] = v
			}
		}
		
	case PairwiseSerializable:
		doUpdate = func(existing map[string]interface{}) {
			logr.Debug("Updating session with PairwiseSerializable")
			pairStream := data.PairStream()
			if pairStream == nil {
				return
			}
			for pair := range pairStream {
				existing[pair.Key] = pair.Value
			}
		}
		
	default:
		doUpdate = func(existing map[string]interface{}) {
			logr.WithField(
				"data type",
				fmt.Sprintf("%T", data),
				).Debug("Updating session with JSON marshalling")
			// Marshal data to JSON
			jsonData, err := json.Marshal(data)
			if err != nil {
				return
			}
			
			// Unmarshal JSON-ified data to map[string]interface{}
			update := make(map[string]interface{})
			err = json.Unmarshal(jsonData, &update)
			if err != nil {
				return
			}
			
			// Update previous session state with new data
			for k, v := range update {
				existing[k] = v
			}
			return
		}
	}
	
	// Unmarshal u.sessionState to map[string]interface{}
	sessionState := make(map[string]interface{})
	err = json.Unmarshal(u.sessionState, &sessionState)
	if err != nil {
		return
	}
	
	logr.WithFields(sessionState).Debug("Session state before update")
	
	// Update previous session state with new data
	doUpdate(sessionState) // may set err
	if err != nil {
		return
	}
	
	logr.WithFields(sessionState).Debug("Updated session state")
	
	// Marshal session state back to u.sessionState
	u.sessionState, err = json.Marshal(sessionState)
	logr.WithField("new value", string(u.sessionState)).Debug("Session state updated")
	return
}

func (db *dbAccess) SaveSessionState(ctx context.Context, u *User) error {
	wrapError := errorWrapper("SaveSessionState")
	stmt, err := db.getStmt(ctx, `
		UPDATE login_session
		SET session_data = $1::JSON
		WHERE session_token = $2
		`)
	if err != nil {
		return wrapError("preparing statement", err)
	}
	result, err := stmt.ExecContext(ctx, u.sessionState, u.sessionToken)
	if err == nil {
		nRows, err := result.RowsAffected()
		if err == nil && nRows > 0 {
			logr.WithField("saved value", string(u.sessionState)).Debug("Session state saved")
		} else if err == nil && nRows == 0 {
			logr.Error("Session state not saved (no records updated)")
		}
	}
	return err
}
