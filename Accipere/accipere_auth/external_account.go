package accipere_auth

// A BonaFides for a OAuth authenticated account
type ExternalAccount struct {
	Id string
	RealName string
}

func (a *ExternalAccount) Name() string {
	return a.RealName
}

func (a *ExternalAccount) RequiresValidation() bool {
	return false
}

func (a *ExternalAccount) PlaintextCredential() string {
	return a.Id
}
