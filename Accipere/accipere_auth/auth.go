package accipere_auth

import (
	"errors"
	"fmt"
)

var (
	ErrInvalidSession = errors.New("the given token was not valid")
)

type BonaFides interface {
	Name() string
	
	// Returns true if the PlaintextCredential needs to be validated against
	// the database to authenticate the user
	RequiresValidation() bool
	
	// Gives the plaintext credential we can check against the database
	PlaintextCredential() string
}

type UnauthenticatedUser struct {
	Email string
}

func (e *UnauthenticatedUser) Error() string {
	return fmt.Sprintf("user %v not authenticated", e.Email)
}
