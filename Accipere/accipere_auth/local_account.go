package accipere_auth

// This is a BonaFides for a local account using password for authentication
type LocalAccount struct {
	Password string
}

func (a *LocalAccount) Name() string {
	return ""
}

func (a *LocalAccount) RequiresValidation() bool {
	return true
}

func (a *LocalAccount) PlaintextCredential() string {
	return a.Password
}
