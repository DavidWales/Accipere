package main

import (
	"bytes"
	"context"
	crand "crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/crypto/acme/autocert"

	auth "./accipere_auth"
	dbpackage "./accipere_db"
	geo "./accipere_geo"
	af "./appframe"
	log "github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/color"
	"github.com/shopspring/decimal"
	//  "google.golang.org/appengine"
	//  "google.golang.org/appengine/user"
)

const httpPort = ":8000"
const httpsPort = ":10443"
const sessionCookieName = "sessionid"
const DbCxnStrEnvvar = "ACCIPERE_DBCXN"
const OcrServerUrlEnvvar = "ACCIPERE_OCR_URL"
const MaxImageSize = 2 * 1000 * 1000
const RecoverStackSize = 4 << 10 // 4 kB

const internalTokenQP = "internal"
const internalUserQP = "user"

//LoginInfo is a struct representing the JSON to be returned by Google
//TODO remove if we aren't using it
type LoginInfo struct {
	ISS string
	SUB string
	AZP string
	AUD string
	IAT string
	EXP string

	Email         string
	EmailVerified string
	Name          string
	PictureURL    string
	GivenName     string
	FamilyName    string
	Locale        string
}

//Coordinates ...
type Coordinates struct {
	Xcoord float64 `json:"X"`
	Ycoord float64 `json:"Y"`
}

//ReceiptInfo ...
type ReceiptInfo struct {
	ID              int         `json:"Id"`
	OtherOwnerEmail string      `json:"OtherOwner"`
	AccountID       int         `json:"Account_id"`
	Account         string      `json:"Account"`
	Date            string      `json:"Date"`
	Location        Coordinates `json:"Location"`
	Total           string      `json:"Total"`
	Notes           string      `json:"Notes"`
}

//ReceiptList TODO maybe remove this struct from the global scope?
type ReceiptList struct {
	Receipts []ReceiptInfo `json:"receipts"`
}

type Webapp struct {
	*af.Webapp
	db dbpackage.DBAccess
	internalToken string
	
	ocrResults OcrResults
}

type OcrResults struct {
	mu sync.Mutex
	items map[int]OcrResult
}

type OcrResult struct {
	UserID int
	valueChan <-chan decimal.Decimal
	expires time.Time
}

var (
	tlsFlag   = flag.Bool("tls", false, "Run server using TLS (HTTPS)")
	debugFlag = flag.Bool("debug", false, "Run server with increased logging")
	ocrImgUrlSecureFlag = flag.Bool("ocr-imgurl-tls", true, "Send a TLS URL to the OCR server")
	ocrImgHostFlag = flag.String("ocr-imgurl-host", "", "Specify the host name to use in the image URL sent to the OCR server")
)

func main() {
	flag.Parse()

	//Connect to database before starting the server
	connectionString := dbcxnStr(
		"dbname=actinides host=104.236.48.171 user=admin password=cs411 port=5432 sslmode=disable",
	)
	dbhandler, err := dbpackage.NewDBAccess(connectionString)
	if err != nil {
		log.WithFields(log.Fields{
			"event": "DB Access",
			"topic": "Connection",
		}).Fatal(err)
	}
	defer dbhandler.Close()

	app := &Webapp{af.NewWebapp(), dbhandler, newInternalToken(), NewOcrResults()}
	configureWebapp(app)
	e := app.Echo
	
	stopJanitor := app.StartJanitor()
	defer stopJanitor()

	if *tlsFlag {
		// Cache certificates
		e.AutoTLSManager.Cache = autocert.DirCache("./.cache")

		// Only request for our hostname
		e.AutoTLSManager.HostPolicy = autocert.HostWhitelist("accipere.ddns.net")

		e.Logger.Fatal(e.StartAutoTLS(httpsPort))
	} else {
		e.Logger.Fatal(e.Start(httpPort))
	}
}

func newInternalToken() string {
	var tokenBytes [16]byte
	_, err := crand.Read(tokenBytes[:])
	if err != nil { panic(err) }
	return hex.EncodeToString(tokenBytes[:])
}

func NewOcrResults() (result OcrResults) {
	result.items = make(map[int]OcrResult)
	return
}

func configureWebapp(app *Webapp) {
	if *debugFlag {
		log.SetLevel(log.DebugLevel)
	}

	app.
		WithTemplateFuncs(af.FuncMap{
			"marshal": func(v interface{}) string {
				a, err := json.Marshal(v)
				if err != nil {
					panic(err)
				}
				return string(a)
			},
		}).
		LoadTemplates("./templates").
		MapAssets(
			af.AssetMapping{VirtualPath: "/JS", FilePath: "served-assets/JS"},
			af.AssetMapping{VirtualPath: "/CSS", FilePath: "served-assets/CSS"},
		)
	e := app.Echo

	// Middleware
	e.Pre(middleware.MethodOverrideWithConfig(middleware.MethodOverrideConfig{
		Getter: middleware.MethodFromForm("_method"),
	}))
	e.Use(recoverMiddleware)
	e.Use(loggingMiddleware)
	e.Use(app.SessionMiddleware(af.SessionMiddlewareConfig{
		LoadSession: app.loadSession,
	}))

	//Serve static HTML for now
	e.Static("/", "static")

	// Handle the landing page request
	e.GET("/", app.handleWelcome)
	e.GET("/index.html", func(c echo.Context) error {
		return c.Redirect(http.StatusSeeOther, "/")
	})
	e.GET("/signout", app.handleSignout)
	e.GET("/userhome", app.handleUserHome)
	e.GET("/heatmap", app.handleHeatmap)

	//Handle token login
	e.POST("/tokenLogin", loginHandler(app.db))

	app.RegisterResource("/Receipt", &ReceiptResource{app}, "Receipt")
	e.GET("/Receipt/:id/img", app.GetReceiptImage)
	e.GET("/Receipt/:id/total_ocr", app.GetReceiptTotalOcr)

	app.RegisterResource("/Group", &ReceiptAccountResource{app}, "ReceiptAccount")

	app.RegisterResource("/Group/:acct_id/Viewer", &AccountViewerResource{app}, "AccountViewer")

	e.GET("/receiptGather", receiptListSendingHandler(app.db))
	e.GET("/receiptInfo", receiptInfoSendingHandler(app.db))
	e.GET("/receiptFilter", receiptFilterHandler(app.db))

	e.POST("/receiptEdit", app.UpdateResourceExt)
}

// Called to indicate that err is not handleable by the HTTP request of c
func unhandledError(err error, c echo.Context) error {
	return c.NoContent(http.StatusInternalServerError)
}

func loggingMiddleware(hf echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		fnlog := log.WithFields(log.Fields{
			"method":       c.Request().Method,
			"matchedRoute": c.Path(),
			"path":         c.Request().URL.EscapedPath(),
			"topic":        "HTTP(S) request",
		})
		timeStart := time.Now()
		fnlog.WithFields(log.Fields{
			"time": timeStart.Format("2006-01-02 15:04:05.123"),
		}).Info("----- REQUEST STARTED -----")
		result := hf(c)
		fnlog.WithFields(log.Fields{
			"elapsedTime":  time.Now().Sub(timeStart),
			"httpResponse": c.Response().Status,
		}).Info("------ REQUEST ENDED ------")
		return result
	}
}

func recoverMiddleware(hf echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		defer func() {
			if r := recover(); r != nil {
				var err error
				switch r := r.(type) {
				case error:
					err = r
				default:
					err = fmt.Errorf("%v", r)
				}
				stack := make([]byte, RecoverStackSize)
				length := runtime.Stack(stack, true)
				log.Infof("%s\n%s\n", color.Red("PANIC RECOVERY"), stack[:length])
				c.Error(err)
				log.Error(err)
				log.WithFields(log.Fields{
					"matchedRoute": c.Path(),
					"path":         c.Request().URL.EscapedPath(),
					"topic":        "HTTP(S) request",
					"time":         time.Now(),
					"httpResponse": c.Response().Status,
				}).Info("----- REQUEST ABORTED -----")
			}
		}()

		return hf(c)
	}
}

func (a *Webapp) StartJanitor() func() {
	ticker := time.NewTicker(time.Duration(30) * time.Second)
	done := make(chan struct{})
	janitor := func() {
		for {
			select {
			case <- ticker.C:
				a.CleanupExpiredOcrResults()
				
			case <- done:
				return
			}
		}
	}
	go janitor()
	return func() {
		ticker.Stop()
		close(done)
	}
}

func (a *Webapp) CleanupExpiredOcrResults() {
	fnlog := log.WithFields(log.Fields{
		"topic": "OCR janiorial",
	})
	fnlog.Debug("OCR janitor running")
	a.ocrResults.mu.Lock()
	defer a.ocrResults.mu.Unlock()
	curTime := time.Now()
	cleanupCount := 0
	for receiptID, ocrResult := range a.ocrResults.items {
		if curTime.After(ocrResult.expires) {
			delete(a.ocrResults.items, receiptID)
			cleanupCount++
		}
	}
	fnlog.WithFields(log.Fields{
		"removedEntryCount": cleanupCount,
	}).Debug("OCR janitorial finished")
}

func startHandlerLog(c echo.Context, handler string) *log.Entry {
	return af.ReqLog(c).WithField("handler", handler)
}

var ErrNoSession = errors.New("No valid session")

func (a *Webapp) loadSession(c echo.Context) (af.Session, error) {
	fnlog := af.ReqLog(c)
	cookie, err := c.Cookie(sessionCookieName)
	if err == http.ErrNoCookie {
		return nil, ErrNoSession
	} else if err != nil {
		fnlog.WithFields(log.Fields{
			"event": "Cookie Access",
			"topic": "Reading",
		})
		fnlog.WithField("topic", "Loading session").Error(err)
		return nil, ErrNoSession
	}

	sessionIDHex := cookie.Value
	sessionIDBytes, err := hex.DecodeString(sessionIDHex)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Decoding",
		}).Debug(err)
		return nil, ErrNoSession
	}

	user, err := a.db.AccessLoginSessionUser(c.Request().Context(), sessionIDBytes)
	if err == auth.ErrInvalidSession {
		fnlog.WithFields(log.Fields{
			"event": "Database Access",
			"topic": "Login Session Access",
		}).Debug(err)
		return nil, ErrNoSession
	} else if err != nil {
		fnlog.WithFields(log.Fields{
			"event": "Database Access",
			"topic": "Login Session Access",
		}).Error(err)
		return nil, ErrNoSession
	}

	fnlog.Data["userID"] = user.Id
	fnlog.WithFields(log.Fields{
		"matchedRoute": c.Path(),
	}).Debug("Session loaded")

	setSessionCookieExpiration(cookie, a.db)
	c.SetCookie(cookie)

	return &Session{a.db, user}, nil
}

func setSessionCookieExpiration(cookie *http.Cookie, db dbpackage.DBAccess) {
	cookie.Expires = time.Now().Add(
		time.Duration(db.TokenValidityMinutes()) * time.Minute,
	)
}

type Session struct {
	Db   dbpackage.DBAccess
	User *dbpackage.User
}

func (s *Session) ReadSessionState(out interface{}) error {
	return s.User.ReadSessionState(out)
}

func (s *Session) WriteSessionState(data interface{}) error {
	return s.User.WriteSessionState(data)
}

func (s *Session) SaveSessionState(c echo.Context) error {
	return s.Db.SaveSessionState(c.Request().Context(), s.User)
}

func (s *Session) Terminate(c echo.Context) error {
	return s.Db.TerminateLoginSessionUser(c.Request().Context(), s.User)
}

var ErrNoUser = errors.New("No user context")

func (a *Webapp) getUser(c echo.Context) (*dbpackage.User, error) {
	fnlog := af.ReqLog(c)
	session := a.Session(c)
	if session, ok := session.(*Session); ok {
		if session.User == nil {
			return nil, ErrNoUser
		}
		return session.User, nil
	}
	fnlog.WithFields(log.Fields{
		"topic":       "Accessing session data",
		"sessionType": fmt.Sprintf("%T", session),
	}).Error("a.Session() is not a Session")
	return nil, ErrNoSession
}

func intParamId(c echo.Context, name string) (int, error) {
	result, err := strconv.Atoi(c.Param(name))
	if err != nil {
		return 0, &echo.HTTPError{400, fmt.Sprintf("%v is not a valid %v", c.Param(name), name)}
	}
	af.ReqLog(c).Data[name] = result
	return result, nil
}

func (a *Webapp) handleWelcome(c echo.Context) error {
	startHandlerLog(c, "Webapp#handleWelcome")
	if a.Session(c) != nil {
		// A valid session token was provided in the cookie, so just
		// redirect to /userhome
		return c.Redirect(http.StatusSeeOther, "/userhome")
	}
	return c.Render(http.StatusOK, "landing.gohtml", nil)
}

func (a *Webapp) handleSignout(c echo.Context) error {
	startHandlerLog(c, "Webapp#handleSignout")
	session := a.Session(c)
	if session != nil {
		session.Terminate(c)
		
		idCookie := new(http.Cookie)
		idCookie.Name = sessionCookieName
		idCookie.Value = ""
		idCookie.Expires = time.Now()
		c.SetCookie(idCookie)
	}
	
	return successRedirect(c, "/")
}

func (a *Webapp) handleUserHome(c echo.Context) error {
	startHandlerLog(c, "Webapp#handleUserHome")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	return c.Render(http.StatusOK, "userhome.gohtml", af.NamedObjects{
		"User": user,
	})
}

func (a *Webapp) handleHeatmap(c echo.Context) error {
	startHandlerLog(c, "Webapp#handleHeatmap")
	return c.Render(http.StatusOK, "heatmap.gohtml", nil)
}

//Wrap check login function so it has access to the dbhandler package
//from http://stackoverflow.com/questions/34046194/how-to-pass-arguments-to-router-handlers-in-golang-using-gin-web-framework
func loginHandler(dbhandler dbpackage.DBAccess) echo.HandlerFunc {
	return func(c echo.Context) error {
		fnlog := log.WithField("handler", "loginHandler")
		token := c.QueryParam("token")
		req, err := http.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token)
		if err != nil {
			fnlog.WithFields(log.Fields{
				"topic": "Google Sign-In",
			}).Error(err)
			return c.NoContent(http.StatusBadGateway)
		}

		//Login failed, redirect to error
		if req.StatusCode != 200 {
			return c.Redirect(http.StatusSeeOther, "/?loginFailed=true")
		}

		//Read JSON
		decoder := json.NewDecoder(req.Body)
		var tokenInfo map[string]interface{}
		err = decoder.Decode(&tokenInfo)
		if err != nil {
			fnlog.WithFields(log.Fields{
				"topic": "Decode Google tokenInfo",
			}).Error(err)
			return unhandledError(err, c)
		}

		//Check if the token is authorized for use with our service
		var tokenAuth = tokenInfo["aud"].(string)
		if tokenAuth != "299966431724-sgfrgd6ie1v7q9acd9jgmfjfkaollia3.apps.googleusercontent.com" {
			return c.Redirect(http.StatusSeeOther, "/?loginFailed=true")
		}

		var fullName = tokenInfo["name"].(string)
		var emailAddr = tokenInfo["email"].(string)
		var googleID = tokenInfo["sub"].(string)

		//Get/set login info
		var tmpAccountInfo = auth.ExternalAccount{googleID, fullName}
		var requestContext = c.Request().Context()
		sessionID, err := dbhandler.CreateLoginSession(requestContext, emailAddr, &tmpAccountInfo)
		if err != nil {
			fnlog.WithFields(log.Fields{
				"topic":  "Login Session",
				"action": "Creation",
			}).Error(err)
			return unhandledError(err, c)
		}

		decodedSessionID := hex.EncodeToString(sessionID)

		//Set cookie
		idCookie := new(http.Cookie)
		idCookie.Name = sessionCookieName
		idCookie.Value = decodedSessionID
		setSessionCookieExpiration(idCookie, dbhandler)
		c.SetCookie(idCookie)

		return c.Redirect(http.StatusSeeOther, "/userhome")
	}
}

type ReceiptResource struct {
	*Webapp
}

func (a *ReceiptResource) CreateResource(c echo.Context) error {
	startHandlerLog(c, "ReceiptResource#CreateResource")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/?loginExpired=true")
	}
	fnlog := af.ReqLog(c)

	imageData, err := readRequestFile(c, "receiptImage", MaxImageSize)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Receiving image",
		}).Error(err)
		return c.String(http.StatusBadRequest, "/userhome")
	} else if imageData == nil {
		fnlog.WithFields(log.Fields{
			"topic": "Receiving image",
		}).Infof("received no image for receipt", len(imageData))
	} else {
		fnlog.WithFields(log.Fields{
			"topic":     "Receiving image",
			"imageSize": len(imageData),
		}).Info("received image for receipt")
	}
	receiptID, err := a.db.SaveReceipt(c.Request().Context(), user.Id,
		geoLocFromRequest(c), []byte(imageData))
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Saving record",
		}).Error(err)
		return c.String(http.StatusInternalServerError, "/userhome")
	} else {
		fnlog = fnlog.WithField("receiptID", receiptID)
		fnlog.WithFields(log.Fields{
			"topic": "Saving record",
		}).Info("saved receipt")
	}
	
	receiptOcrTask, err := NewOcrTask(a.Webapp, c, receiptID)
	if err == nil {
		go receiptOcrTask.parseTotalFromImage()
	} else {
		fnlog.WithFields(log.Fields{
			"topic": "OCR task",
		}).Error(err)
	}

	return c.String(
		http.StatusOK,
		a.ResourceURIBuilder("Receipt", c).EditInstance(receiptID),
	)
}

type OcrTask struct {
	*Webapp
	ReceiptID int
	ReceiptURL string
	Log log.FieldLogger
	ResultChan chan<- decimal.Decimal
}

func NewOcrTask(a *Webapp, c echo.Context, receiptID int) (result *OcrTask, err error) {
	// Panic should just return an error
	defer func() {
		if r := recover(); r != nil {
			if rErr, is_error := r.(error); is_error {
				err = rErr
			} else {
				err = fmt.Errorf("%v", r)
			}
		}
	}()
	
	resultChan := make(chan decimal.Decimal, 1)
	
	receiptUris := a.ResourceURIBuilder("Receipt", c)
	user, err := a.getUser(c)
	if err != nil {
		return
	}
	imgHost := c.Request().Host
	if len(*ocrImgHostFlag) > 0 {
		imgHost = *ocrImgHostFlag
	}
	
	url := "http"
	if *ocrImgUrlSecureFlag {
		url += "s"
	}
	url += "://" + imgHost + receiptUris.Instance(receiptID) + "/img"
	url += "?" + internalTokenQP + "=" + a.internalToken
	url += "&" + internalUserQP + "=" + strconv.Itoa(user.Id)
	result = &OcrTask{
		Webapp: a,
		ReceiptID: receiptID,
		ReceiptURL: url,
		Log: log.WithFields(log.Fields{
			"userID": user.Id,
			"receiptID": receiptID,
		}),
		ResultChan: resultChan,
	}
	a.publishOcrResultChan(user.Id, receiptID, resultChan)
	return
}

func (a *Webapp) publishOcrResultChan(userID, receiptID int, resultChan <-chan decimal.Decimal)  {
	ocr := OcrResult{
		userID,
		resultChan,
		time.Now().Add(time.Duration(5) * time.Minute),
	}
	
	a.ocrResults.mu.Lock()
	defer a.ocrResults.mu.Unlock()
	a.ocrResults.items[receiptID] = ocr
}

func (a *Webapp) getOcrResult(receiptID int) (OcrResult, error) {
	a.ocrResults.mu.Lock()
	defer a.ocrResults.mu.Unlock()
	result, ok := a.ocrResults.items[receiptID]
	if ok {
		return result, nil
	} else {
		return OcrResult{}, fmt.Errorf("OCR result not found for receipt %v", receiptID)
	}
}

func (t *OcrTask) parseTotalFromImage() {
	fnlog := t.Log.WithFields(log.Fields{
		"topic": "Image OCR",
	})
	
	defer func() {
		close(t.ResultChan)
	}()
	
	taskCtx, taskCancel := context.WithTimeout(
		context.Background(),
		time.Duration(90) * time.Second,
	)
	defer taskCancel()
	
	// Construct the request body
	reqBody, err := json.Marshal(map[string]interface{}{
		"engine": "tesseract",
		"img_url": t.ReceiptURL,
	})
	if err != nil {
		fnlog.WithError(err).Error("OCR request -> JSON failed")
		return
	}
	fnlog.WithFields(log.Fields{
		"value": string(reqBody),
	}).Debug("OCR request body")
	req, err := http.NewRequest(
		"POST",
		getOcrServerURL(),
		bytes.NewReader(reqBody),
	)
	// Use the task context for this request (to impose timeout)
	req = req.WithContext(taskCtx)
	// Set the header with the right Content-Type
	req.Header.Set("Content-Type", "application/json")
	// Send request to OCR server
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fnlog.WithError(err).Error("OCR HTTP request failed")
		return
	}
	fnlog.WithFields(log.Fields{
		"responseStatus": resp.Status,
	}).Info("OCR done")
	
	if resp.StatusCode == http.StatusOK {
		t.saveOcrResult(resp.Body)
		return
	} else {
		fnlog.WithFields(log.Fields{
			"HTTP response": resp.Status,
		}).Error("OCR failed")
		return
	}
}

func (t *OcrTask) saveOcrResult(ocrResult io.ReadCloser)  {
	fnlog := t.Log.WithFields(log.Fields{
		"topic": "OCR result parsing/saving",
	})
	var contentBuf [4096]byte
	var nReceived int
	var err error
	content := contentBuf[:]
	
	for err == nil {
		fnlog.Debug("Reading an OCR chunk")
		nRead, err := ocrResult.Read(content)
		fnlog.WithFields(log.Fields{
			"bytesRead": nRead,
		}).Debug("Chunk read from OCR result")
		if err == io.EOF || nRead == 0{
			nReceived += nRead
			break
		} else if err != nil {
			fnlog.WithError(err).Error("Receiving OCR result")
			return
		} else {
			content = content[nRead:]
			nReceived += nRead
		}
	}
	content = contentBuf[:nReceived]
	fnlog.WithFields(log.Fields{
		"length": len(content),
	}).Debug("OCR content received")
	
	// TODO Parse largest number from content
	total, err := t.parseTotalFromOcrText(content)
	if err != nil {
		fnlog.WithError(err).Info("Parsing OCR total failed")
		return
	}
	fnlog.WithFields(log.Fields{
		"receiptOcrTotal": total,
	}).Debug("OCR total computed")
	
	// Send the result value
	t.ResultChan <- total
}

var ocrAmountPattern = regexp.MustCompile(`\d(,?\d)*\.\d\d`)

func (t *OcrTask) parseTotalFromOcrText(text []byte) (decimal.Decimal, error) {
	matchStrs := ocrAmountPattern.FindAll(text, -1)
	var maxAmount decimal.Decimal
	
	t.Log.WithFields(log.Fields{
		"count": len(matchStrs),
	}).Debug("amounts found")
	
	for _, amtStr := range matchStrs {
		cleanAmtStr := strings.Replace(string(amtStr), ",", "", -1)
		amt, err := decimal.NewFromString(cleanAmtStr)
		if err == nil && maxAmount.Cmp(amt) < 0 {
			maxAmount = amt
		}
	}
	
	if maxAmount != decimal.Zero {
		return maxAmount, nil
	}
	
	return decimal.Zero, fmt.Errorf("No total found")
}

func readRequestFile(c echo.Context, name string, limit int) ([]byte, error) {
	header, err := c.FormFile(name)
	if err != nil {
		return nil, nil
	}
	file, err := header.Open()
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// partially from http://stackoverflow.com/a/38176128/160072
	buff := bytes.NewBuffer(make([]byte, 0, limit+1))
	length, err := buff.ReadFrom(io.LimitReader(file, int64(limit+1)))
	if err != nil {
		return nil, err
	}
	if int64(limit) < length {
		return nil, ErrOversizeFile{int64(limit)}
	}
	return buff.Bytes(), nil
}

type ErrOversizeFile struct {
	SizeLimit int64
}

func (e ErrOversizeFile) Error() string {
	return fmt.Sprintf("file exceeded %v bytes", e.SizeLimit)
}

func geoLocFromRequest(c echo.Context) geo.Point {
	longitude, err := strconv.ParseFloat(c.FormValue("lon"), 64)
	if err != nil {
		return nil
	}
	latitude, err := strconv.ParseFloat(c.FormValue("lat"), 64)
	if err != nil {
		return nil
	}
	return &geo.LongLat{longitude, latitude}
}

func (a *ReceiptResource) ShowResource(c echo.Context) error {
	startHandlerLog(c, "ReceiptResource#ShowResource")
	return a.getView(c, "receipt.gohtml", false)
}

func (a *Webapp) GetReceiptImage(c echo.Context) error {
	startHandlerLog(c, "Webapp#GetReceiptImage")
	
	var userID int
	var err error
	
	iqToken := c.QueryParam(internalTokenQP)
	if iqToken == a.internalToken {
		userID, err = strconv.Atoi(c.QueryParam(internalUserQP))
		if err != nil {
			return err
		}
	} else {
		user, err := a.getUser(c)
		if err != nil {
			return err
		}
		userID = user.Id
	}
	
	fnlog := af.ReqLog(c)
	receiptID, err := intParamId(c, "id")
	if err != nil {
		return err
	}
	
	content, err := a.db.GetReceiptImage(c.Request().Context(), userID, receiptID)
	if err == dbpackage.ErrNoReceipt {
		fnlog.WithFields(log.Fields{
			"topic": "Retrieving image",
		}).Info("no such receipt")
		return c.NoContent(http.StatusNotFound)
	}
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Retrieving image",
		}).Error(err)
		return unhandledError(err, c)
	}

	return c.Blob(http.StatusOK, "image/jpeg", content)
}

func (a *Webapp) GetReceiptTotalOcr(c echo.Context) error {
	startHandlerLog(c, "Webapp#GetReceiptTotalOcr")
	user, err := a.getUser(c)
	if err != nil {
		return err
	}
	fnlog := af.ReqLog(c)
	receiptID, err := intParamId(c, "id")
	if err != nil {
		return err
	}
	
	ocrWaitContext, _ := context.WithTimeout(
		c.Request().Context(),
		time.Duration(90) * time.Second,
	)
	
	ocrResult, err := a.getOcrResult(receiptID)
	if err != nil || ocrResult.UserID != user.Id {
		return c.String(http.StatusNotFound, "Unknown receipt")
	}
	
	select {
	case <- ocrWaitContext.Done():
		fnlog.Warning("Timeout")
		return c.String(http.StatusNotFound, "Timeout")
		
	case result, ok := <- ocrResult.valueChan:
		if ok {
			return c.String(http.StatusOK, result.String())
		} else {
			return c.String(http.StatusNotFound, "Timeout")
		}
	}
}

func (a *ReceiptResource) GetResourceEditingForm(c echo.Context) error {
	startHandlerLog(c, "ReceiptResource#GetResourceEditingForm")
	return a.getView(c, "receipt.edit.gohtml", true)
}

func (a *ReceiptResource) getView(c echo.Context, view string, includeAccounts bool) error {
	fnlog := af.ReqLog(c)
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	receiptID, err := intParamId(c, "id")
	if err != nil {
		return err
	}
	receipt, err := a.db.GetReceiptInfo(c.Request().Context(), user.Id, receiptID)
	if err == dbpackage.ErrNoReceipt {
		return &echo.HTTPError{404, "No such receipt"}
	}
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Retrieving receipt from database",
		}).Error(err)
		return err
	}
	pageContext := af.NamedObjects{
		"Receipt": remodelReceipt(receipt, user.Id),
	}
	if includeAccounts {
		accountList, err := a.db.GetReceiptAccountList(c.Request().Context(), user.Id, nil)
		if err == nil {
			acctNames := make([]string, len(accountList))
			for i, acct := range accountList {
				acctNames[i] = acct.Name
			}
			pageContext["ReceiptGroups"] = acctNames
		} else {
			fnlog.WithFields(log.Fields{
				"topic": "Getting user's receipt groups",
			}).Error(err)
		}
	}
	return c.Render(http.StatusOK, view, pageContext)
}

func (a *Webapp) UpdateResourceExt(c echo.Context) error {
	startHandlerLog(c, "Webapp#UpdateResourceExt")
	user, err := a.getUser(c)
	if err != nil {
		return err
	}
	fnlog := af.ReqLog(c)
	
	receiptID, err := strconv.Atoi(c.FormValue("id"))
	if err != nil {
		return err
	}

	receiptTotal := c.FormValue("total")
	receiptNotes := c.FormValue("notes")
	receiptDate := c.FormValue("date")
	
	receiptLat, err := strconv.ParseFloat(c.FormValue("latitude"), 64)
	if err != nil {
		return err
	}
	
	receiptLng, err := strconv.ParseFloat(c.FormValue("longitude"), 64)
	if err != nil {
		return err
	}

	err = a.db.SetReceiptFields(c.Request().Context(), user.Id, receiptID,
		dbpackage.ReceiptFieldAssignment{"total", receiptTotal},
		dbpackage.ReceiptFieldAssignment{"notes", receiptNotes},
		dbpackage.ReceiptFieldAssignment{"receipt_date", receiptDate},
		dbpackage.ReceiptFieldAssignment{"location", &geo.LongLat{receiptLng, receiptLat}},
	)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic":       "Updating receipt fields",
			"total":       receiptTotal,
			"notesLength": len(receiptNotes),
			"date": receiptDate,
			"latitude": receiptLat,
			"longitude": receiptLng,
		}).Error(err)
		return unhandledError(err, c)
	} else {
		fnlog.WithFields(log.Fields{
			"topic":       "Updating receipt fields",
			"total":       receiptTotal,
			"notesLength": len(receiptNotes),
			"date": receiptDate,
			"latitude": receiptLat,
			"longitude": receiptLng,
		}).Info("updated receipt")
	}

	receiptAcc := c.FormValue("account")
	if len(receiptAcc) > 0 {
		acctSet, err := a.db.SetReceiptAccount(c.Request().Context(),
			user.Id, receiptID, receiptAcc)
		if err != nil {
			fnlog.WithFields(log.Fields{
				"topic": "Receipt account",
			}).Warn(err)
		} else if acctSet != receiptAcc {
			fnlog.WithFields(log.Fields{
				"topic":            "Receipt account",
				"requestedAccount": receiptAcc,
			}).Warn("account does not exist")
			af.FlashWarning(c, fmt.Sprintf(`Group "%v" doesn't exist; receipt not added to any group`, receiptAcc))
		} else {
			fnlog.WithFields(log.Fields{
				"topic":   "Receipt account",
				"account": receiptAcc,
			}).Info("receipt listed in account")
		}
	}
	
	return nil
}

func (a *ReceiptResource) UpdateResource(c echo.Context) error {
	startHandlerLog(c, "ReceiptResource#UpdateResource")
	user, err := a.getUser(c)
	if err != nil {
		return err
	}
	fnlog := af.ReqLog(c)
	receiptID, err := intParamId(c, "id")
	if err != nil {
		return err
	}

	receiptTotal := c.FormValue("rtotal")
	receiptNotes := c.FormValue("rnotes")
	
	var assignments []dbpackage.ReceiptFieldAssignment
	{
		include := func(asgnmt dbpackage.ReceiptFieldAssignment) {
			assignments = append(assignments, asgnmt)
		}
		
		if len(receiptTotal) > 0 {
			include(dbpackage.RFTotal.Gets(receiptTotal))
		} else {
			include(dbpackage.RFTotal.Gets(decimal.NullDecimal{}))
		}
		include(dbpackage.RFNotes.Gets(receiptNotes))
	}
	err = a.db.SetReceiptFields(c.Request().Context(), user.Id, receiptID,
		assignments...,
	)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic":       "Updating receipt fields",
			"total":       receiptTotal,
			"notesLength": len(receiptNotes),
		}).Error(err)
		return unhandledError(err, c)
	} else {
		fnlog.WithFields(log.Fields{
			"topic":       "Updating receipt fields",
			"total":       receiptTotal,
			"notesLength": len(receiptNotes),
		}).Info("updated receipt")
	}

	receiptAccount := c.FormValue("account")
	acctSet, err := a.db.SetReceiptAccount(c.Request().Context(),
		user.Id, receiptID, receiptAccount)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Receipt account",
		}).Warn(err)
	} else if acctSet != receiptAccount {
		fnlog.WithFields(log.Fields{
			"topic":            "Receipt account",
			"requestedAccount": receiptAccount,
		}).Warn("account does not exist")
		af.FlashWarning(c, fmt.Sprintf(`Group "%v" doesn't exist; receipt not added to any group`, receiptAccount))
	} else {
		fnlog.WithFields(log.Fields{
			"topic":   "Receipt account",
			"account": receiptAccount,
		}).Info("receipt listed in account")
	}

	af.FlashSuccess(c, "Receipt details updated")
	return successRedirect(c, "/userhome")
}

func (a *ReceiptResource) DeleteResource(c echo.Context) error {
	startHandlerLog(c, "ReceiptResource#DeleteResource")
	user, err := a.getUser(c)
	if err != nil {
		return err
	}
	receiptID, err := intParamId(c, "id")
	if err != nil {
		return err
	}
	fnlog := af.ReqLog(c)
	err = a.db.DeleteReceipt(c.Request().Context(), user.Id, receiptID)
	if err == dbpackage.ErrNoReceipt {
		af.FlashError(c, "The receipt specified for deletion does not exist")
	} else if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Deleting receipt in database",
		}).Error(err)
		return err
	}
	if err == nil {
		af.FlashSuccess(c, "Receipt deleted")
	}
	return successRedirect(c, "/userhome")
}

type ReceiptAccountResource struct {
	*Webapp
}

func (a *ReceiptAccountResource) ResourceIndex(c echo.Context) error {
	startHandlerLog(c, "ReceiptAccountResource#ResourceIndex")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	fnlog := af.ReqLog(c)
	acctList, err := a.db.GetReceiptAccountList(c.Request().Context(), user.Id, nil)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Index retrieval",
		}).Error(err)
		return err
	}
	accountNames := make([]string, len(acctList))
	for i, acct := range acctList {
		accountNames[i] = acct.Name
	}
	return c.Render(http.StatusOK, "account.index.gohtml", af.NamedObjects{
		"AccountNames": accountNames,
	})
}

func (a *ReceiptAccountResource) ShowResource(c echo.Context) error {
	startHandlerLog(c, "ReceiptAccountResource#ShowResource")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	fnlog := af.ReqLog(c)
	accountName := c.Param("id")
	viewerList, err := a.db.GetAccountViewers(c.Request().Context(), user.Id, accountName, nil)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Viewer list retreival",
		}).Error(err)
		return err
	}
	return c.Render(http.StatusOK, "account.gohtml", map[string]interface{}{
		"Account": accountName,
		"Viewers": viewerList,
	})
}

func (a *ReceiptAccountResource) GetNewResourceForm(c echo.Context) error {
	startHandlerLog(c, "ReceiptAccountResource#GetNewResourceForm")
	return c.Render(http.StatusOK, "account.new.gohtml", nil)
}

func (a *ReceiptAccountResource) CreateResource(c echo.Context) error {
	startHandlerLog(c, "ReceiptAccountResource#CreateResource")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	fnlog := af.ReqLog(c)
	accountName := c.FormValue("name")
	// Look out for special cases -- these are special names
	switch accountName {
	case "":
		af.FlashError(c, "The group name may not be empty")
		if isXHR(c) {
			return c.String(http.StatusBadRequest, "Invalid group name")
		}
		return c.Redirect(http.StatusSeeOther, a.ResourceURIBuilder("ReceiptAccount", c).Collection())
	case "new":
		af.FlashError(c, fmt.Sprintf(`The name "%v" is not allowed as a group name`, accountName))
		if isXHR(c) {
			return c.String(http.StatusBadRequest, "Invalid group name")
		}
		return c.Redirect(http.StatusSeeOther, a.ResourceURIBuilder("ReceiptAccount", c).Collection())
	}
	_, err = a.db.CreateReceiptAccount(c.Request().Context(), user.Id, accountName)
	if err == dbpackage.ErrAccountAlreadyExists {
		af.FlashWarning(c, fmt.Sprintf(`Receipt group "%v" already exists`, accountName))
	} else if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Receipt account creation",
		}).Error(err)
		return err
	}
	if err == nil {
		af.FlashSuccess(c, fmt.Sprintf(`Receipt group "%v" created`, accountName))
	}
	return successRedirect(c, a.ResourceURIBuilder("ReceiptAccount", c).Instance(accountName))
}

func (a *ReceiptAccountResource) DeleteResource(c echo.Context) error {
	startHandlerLog(c, "ReceiptAccountResource#DeleteResource")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	fnlog := af.ReqLog(c)
	accountName := c.Param("id")
	err = a.db.DeleteReceiptAccount(c.Request().Context(), user.Id, accountName)
	if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Deleting receipt account in the database",
		}).Error(err)
		return err
	}
	af.FlashSuccess(c, fmt.Sprintf(`Receipt group "%v" deleted; all receipts previously in that account are in no account`, accountName))
	return successRedirect(c, a.ResourceURIBuilder("ReceiptAccount", c).Collection())
}

type AccountViewerResource struct {
	*Webapp
}

func (a *AccountViewerResource) GetNewResourceForm(c echo.Context) error {
	startHandlerLog(c, "AccountViewerResource#GetNewResourceForm")
	return c.Render(http.StatusOK, "account_viewer.new.gohtml", map[string]interface{}{
		"Account": c.Param("acct_id"),
	})
}

func (a *AccountViewerResource) CreateResource(c echo.Context) error {
	startHandlerLog(c, "AccountViewerResource#CreateResource")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	accountName := c.Param("acct_id")
	targetUserEmail := c.FormValue("user_email")
	fnlog := af.ReqLog(c).WithFields(log.Fields{
		"account":     accountName,
		"targetEmail": targetUserEmail,
	})
	err = a.db.AllowUserToViewAccount(c.Request().Context(), user.Id, accountName, targetUserEmail)
	if err == dbpackage.ErrNoViewingChange {
		af.FlashWarning(c, fmt.Sprintf(`User "%v" could not be added to group "%v"`, targetUserEmail, accountName))
	} else if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Allowing view of group in database",
		}).Error(err)
		return err
	}
	if err == nil {
		af.FlashSuccess(c, fmt.Sprintf(`User "%v" now allowed to view receipts in group "%v"`, targetUserEmail, accountName))
	}
	return successRedirect(c, a.ResourceURIBuilder("ReceiptAccount", c).Instance())
}

func (a *AccountViewerResource) DeleteResource(c echo.Context) error {
	startHandlerLog(c, "AccountViewerResource#DeleteResource")
	user, err := a.getUser(c)
	if err != nil {
		return c.Redirect(http.StatusSeeOther, "/")
	}
	accountName := c.Param("acct_id")
	targetUserEmail := c.Param("id")
	fnlog := af.ReqLog(c).WithFields(log.Fields{
		"account":     accountName,
		"targetEmail": targetUserEmail,
	})
	err = a.db.ForbidUserToViewAccount(c.Request().Context(), user.Id, accountName, targetUserEmail)
	if err == dbpackage.ErrNoViewingChange {
		af.FlashWarning(c, fmt.Sprintf(`User "%v" was not removed from viewing this group`, targetUserEmail))
	} else if err != nil {
		fnlog.WithFields(log.Fields{
			"topic": "Disallowing view of group in database",
		}).Error(err)
		return err
	}
	return successRedirect(c, a.ResourceURIBuilder("ReceiptAccount", c).Instance())
}

func receiptInfoSendingHandler(dbhandler dbpackage.DBAccess) echo.HandlerFunc {
	return func(c echo.Context) error {
		fnlog := log.WithField("handler", "receiptInfoSendingHandler")
		userID, err := getUserID(dbhandler, c)
		if err != nil || userID < 0 {
			log.WithFields(log.Fields{
				"event": "UserID retrieval",
				"topic": "Login Session Access",
			}).Info("Session (probably) Expired")
			return c.Redirect(http.StatusSeeOther, "index.html?loginExpired=true")
		}
		
		receiptID, err := strconv.Atoi(c.FormValue("id"))
		if err != nil {
			return err
		}

		rawReceiptInfo, err := dbhandler.GetReceiptInfo(c.Request().Context(), userID, receiptID)
		if err != nil {
			fnlog.WithFields(log.Fields{
				"topic": "Receipt Info Retrieval",
			}).Error(err)
			return unhandledError(err, c)
		}

		var receiptInfo = remodelReceipt(rawReceiptInfo, userID)
		
		return c.JSON(http.StatusOK, receiptInfo)
	}
}

func receiptListSendingHandler(dbhandler dbpackage.DBAccess) echo.HandlerFunc {
	return func(c echo.Context) error {
		fnlog := log.WithField("handler", "receiptListSendingHandler")
		userID, err := getUserID(dbhandler, c)
		if err != nil || userID < 0 {
			log.WithFields(log.Fields{
				"event": "UserID retreival",
				"topic": "Login Session Access",
			}).Info("Session (probably) Expired")
			return c.Redirect(http.StatusSeeOther, "index.html?loginExpired=true")
		}

		rawReceiptList, err := dbhandler.GetReceiptList(c.Request().Context(), userID, nil)
		if err != nil {
			fnlog.WithFields(log.Fields{
				"topic": "Receipt List Retrieval",
			}).Error(err)
			return unhandledError(err, c)
		}

		var currReceiptList ReceiptList

		for _, receipt := range rawReceiptList {
			currReceiptList.Receipts = append(currReceiptList.Receipts,
				remodelReceipt(&receipt, userID))
		}
		return c.JSON(http.StatusOK, currReceiptList)
	}
}

func receiptFilterHandler(dbhandler dbpackage.DBAccess) echo.HandlerFunc {
	return func(c echo.Context) error {
		fnlog := log.WithField("handler", "receiptFilterHandler")
		userID, err := getUserID(dbhandler, c)
		if err != nil || userID < 0 {
			log.WithFields(log.Fields{
				"event": "UserID retreival",
				"topic": "Login Session Access",
			}).Info("Session (probably) Expired")
			return c.Redirect(http.StatusSeeOther, "index.html?loginExpired=true")
		}

		filterTerms := make([]dbpackage.SqlClause, 0)

		//Handle cost range
		{
			specifiedMinCostString := c.QueryParam("costMin")
			specifiedMaxCostString := c.QueryParam("costMax")

			//If min and max costs are null, don't filter on them
			if specifiedMinCostString != "" || specifiedMaxCostString != "" {
				filterTerm := &dbpackage.FilterByRange{
					ExprOfTuple: string(dbpackage.RFTotal),
				}
				if specifiedMinCostString != "" {
					filterTerm.RangeStart = specifiedMinCostString;
				}
				if specifiedMaxCostString != "" {
					filterTerm.RangeEnd = specifiedMaxCostString;
				}

				filterTerms = append(filterTerms, filterTerm)
			}
		}
		//
		//
		//Handle Date Range
		{
			specifiedMinDateString := c.QueryParam("startDate")
			specifiedMaxDateString := c.QueryParam("endDate")
			
			filterTerm := &dbpackage.FilterByRange{
				ExprOfTuple: string(dbpackage.RFDate),
			}
			if specifiedMinDateString != "" {
				filterTerm.RangeStart = specifiedMinDateString
			}
			if specifiedMaxDateString != "" {
				filterTerm.RangeEnd = specifiedMaxDateString
			}
			filterTerms = append(filterTerms, filterTerm)
		}

		//
		//
		//Handle distance filtering
		var refPoint geo.Point
		{
			distanceAwayString := c.QueryParam("distance")
			if distanceAwayString != "" {
				distanceAway, err := strconv.ParseFloat(distanceAwayString, 64)
				if err != nil {
					log.WithFields(log.Fields{
						"event": "Distance parsing",
					}).Info("Invalid distance")
					distanceAway = 0.0
				}

				latitudeString := c.QueryParam("latitude")
				longitudeString := c.QueryParam("longitude")

				longitude, err := strconv.ParseFloat(longitudeString, 64)
				if err != nil {
					log.WithFields(log.Fields{
						"event": "Longitude parsing",
					}).Info("Invalid longitude")
					longitude = 0.0
				}
				latitude, err := strconv.ParseFloat(latitudeString, 64)
				if err != nil {
					log.WithFields(log.Fields{
						"event": "Latitude parsing",
					}).Info("Invalid latitude")
				}
				location := &geo.LongLat{longitude, latitude}
				refPoint = location

				distanceExprOfTuple := string(dbpackage.RFLocation)
				distanceFilter := dbpackage.FilterByDistance{distanceExprOfTuple, location, distanceAway}
				filterTerms = append(filterTerms, &distanceFilter)
			}
		}

		var queryFilter = dbpackage.QueryCustomization{}
		if len(filterTerms) > 1 {
			queryFilter.Filter = &dbpackage.FilterIntersection{filterTerms}
		} else if len(filterTerms) == 1 {
			queryFilter.Filter = filterTerms[0]
		}
		
		{
			orderString := c.QueryParam("order")
			switch orderString {
			case "date":
				queryFilter.Ordering = &dbpackage.OrderingHierarchy{
					[]dbpackage.SqlClause{
						&dbpackage.OrderByStoredData{string(dbpackage.RFDate), true},
						&dbpackage.OrderByStoredData{"id", true},
					},
				}
				
			case "cost":
				queryFilter.Ordering = &dbpackage.OrderingHierarchy{
					[]dbpackage.SqlClause{
						&dbpackage.OrderByStoredData{string(dbpackage.RFTotal), true},
						&dbpackage.OrderByStoredData{"id", true},
					},
				}
				
			case "distance":
				if refPoint != nil {
					queryFilter.Ordering = &dbpackage.OrderByDistance{
						ExprOfTuple: string(dbpackage.RFLocation),
						QueryPoint: refPoint,
					}
				}
			}
		}

		rawReceiptList, err := dbhandler.GetCustomizedReceiptList(c.Request().Context(), userID, &queryFilter, nil)
		println("\n\n\n\n\n\n\n\n\n\n\n\n\n")
		fmt.Printf("%v\n", rawReceiptList)
		if err != nil {
			fnlog.WithFields(log.Fields{
				"topic": "Receipt List Retrieval For Filtering",
			}).Error(err)
			return unhandledError(err, c)
		}

		var currReceiptList ReceiptList

		for _, receipt := range rawReceiptList {
			currReceiptList.Receipts = append(currReceiptList.Receipts,
				remodelReceipt(&receipt, userID))
		}
		return c.JSON(http.StatusOK, currReceiptList)
	}
}

func remodelReceipt(receipt *dbpackage.Receipt, viewingUserId int) (currReceipt ReceiptInfo) {
	currReceipt.ID = receipt.Id
	currReceipt.AccountID = int(receipt.Account_id.Int64)
	currReceipt.Account = receipt.Account
	currReceipt.Date = receipt.Date.Format("2006-01-02")
	currReceipt.Notes = receipt.Notes
	if receipt.OwnerId == viewingUserId {
		currReceipt.OtherOwnerEmail = ""
	} else {
		currReceipt.OtherOwnerEmail = receipt.OwnerEmail
	}

	tempLoc := receipt.Location
	if tempLoc.Valid {
		currReceipt.Location.Xcoord = tempLoc.LongLat.Latitude
		currReceipt.Location.Ycoord = tempLoc.LongLat.Longitude
	} else {
		currReceipt.Location.Xcoord = 0
		currReceipt.Location.Ycoord = 0
	}

	tempTotal := receipt.Total
	if tempTotal.Valid {
		currReceipt.Total = tempTotal.Decimal.StringFixed(2)
	} else {
		currReceipt.Total = ""
	}
	return
}

//Handle getting the current session ID
//TODO deal with expired sessions
func getUserID(dbhandler dbpackage.DBAccess, c echo.Context) (int, error) {
	cookie, err := c.Cookie(sessionCookieName)
	if err != nil {
		log.WithFields(log.Fields{
			"event": "Cookie Access",
			"topic": "Reading",
		}).Info(err)
		return -1, err
	}

	sessionIDHex := cookie.Value
	sessionIDBytes, err := hex.DecodeString(sessionIDHex)
	if err != nil {
		log.WithFields(log.Fields{
			"event": "Cookie Access",
			"topic": "Decoding",
		}).Info(err)
		return -1, err
	}

	//TODO handle error and login expiration separately
	userid, err := dbhandler.AccessLoginSession(c.Request().Context(), sessionIDBytes)
	if err != nil {
		log.WithFields(log.Fields{
			"event": "Database Access",
			"topic": "Login Session Access",
		}).Info("Session Expired")
		return -1, err
	}
	return userid, nil
}

func dbcxnStr(defaultVal string) string {
	envVal := os.Getenv(DbCxnStrEnvvar)
	if len(envVal) > 0 {
		return envVal
	}
	return defaultVal
}

func getOcrServerURL() string {
	envVal := os.Getenv(OcrServerUrlEnvvar)
	if len(envVal) > 0 {
		return envVal
	}
	return "http://104.236.48.171:9292/ocr"
}

func isXHR(c echo.Context) bool {
	return len(c.Request().Header.Get("X-Requested-With")) > 0
}

func successRedirect(c echo.Context, uri string) error {
	if isXHR(c) {
		return c.String(http.StatusOK, uri)
	}
	return c.Redirect(http.StatusSeeOther, uri)
}
