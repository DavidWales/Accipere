package accipere_geo

import (
	"database/sql/driver"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type Point interface {
	PointGeometryString() string
}

type LongLat struct {
	Longitude float64
	Latitude float64
}

type NullLongLat struct {
	LongLat LongLat
	Valid bool
}

var pointParser = regexp.MustCompile(
	`^POINT\((?P<long>-?\d{1,3}(?:\.\d+)?) (?P<lat>-?\d{1,2}(?:\.\d+)?)\)$`,
)

var invalidLongLat = fmt.Errorf("invalid LongLat format")

func (nll *NullLongLat) Scan(value interface{}) error {
	if value == nil {
		nll.LongLat, nll.Valid = LongLat{}, false
		return nil
	}
	switch value := value.(type) {
	case string:
		parts := pointParser.FindStringSubmatch(value)
		if parts == nil {
			return invalidLongLat
		}
		var err error
		nll.LongLat.Longitude, err = strconv.ParseFloat(parts[1], 64)
		if err != nil {
			return invalidLongLat
		}
		nll.LongLat.Latitude, err = strconv.ParseFloat(parts[2], 64)
		if err != nil {
			return invalidLongLat
		}
		nll.Valid = true
		return nil
	default:
		return invalidLongLat
	}
}

func (nll *NullLongLat) Value() (driver.Value, error) {
	if nll == nil || !nll.Valid {
		return nil, nil
	}
	
	return nll.LongLat.PointGeometryString(), nil
}

func (pt *LongLat) PointGeometryString() string {
	return fmt.Sprintf("SRID=4326;POINT(%v %v)", pt.Longitude, pt.Latitude)
}

func (pt *LongLat) String() string {
	var lat, long string
	if pt.Latitude >= 0 {
		lat = fmt.Sprintf("%.6f°N", pt.Latitude)
	} else {
		lat = fmt.Sprintf("%.6f°S", -pt.Latitude)
	}
	if pt.Longitude <= 0 {
		long = fmt.Sprintf("%.6f°W", -pt.Longitude)
	} else {
		long = fmt.Sprintf("%.6f°E", pt.Longitude)
	}
	
	return fmt.Sprintf("%v %v", lat, long)
}

// Parse a string in "longitude,latitude" form
func ParseLocation(locStr string) (*LongLat, error) {
	if len(locStr) == 0 {
		return nil, nil
	}
	
	parts := strings.SplitN(locStr, ",", 3)
	if len(parts) != 2 {
		return nil, fmt.Errorf(`location must be "longitude,latitude"`)
	}
	
	var err error
	result := new(LongLat)
	result.Longitude, err = strconv.ParseFloat(parts[0], 64)
	if err != nil {
		return nil, err
	}
	result.Latitude, err = strconv.ParseFloat(parts[1], 64)
	if err != nil {
		return nil, err
	}
	
	return result, nil
}
