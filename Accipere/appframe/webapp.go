package appframe

import (
	"html/template"

	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
)

const CSRFContextKey = "csrf"
const CSRFFormFieldName = "_csrf"
const FlashContextKey = "flash"
const FlashMessengerContextKey = "appframe.messageByFlash"
const FlashSetterContextKey = "appframe.setFlash"
const templateExt = ".gohtml"
const requestLoggerKey = "RequestLogger"

var log = logrus.StandardLogger()

type Webapp struct {
	// Entries in this map are included via Funcs when compiling templates
	TemplateFuncs map[string]interface{}

	templates map[string]*template.Template
	assets map[string]AssetInfo
	assetURIs map[string]struct{}
	resources ResourcesMap
	Echo *echo.Echo
	sessionContextKey string
}

type ResourcesMap map[string]ResourceInfo

type ResourceInfo struct {
	Resource interface{}
	URIPattern string
}

type AssetInfo struct {
	PublicName string
}

func NewWebapp() *Webapp {
	result := new(Webapp)
	result.TemplateFuncs = make(map[string]interface{})
	result.templates = make(map[string]*template.Template)
	result.Echo = echo.New()
	result.Echo.Renderer = result
	
	return result
}

func (a *Webapp) resourceURIBuilders(c echo.Context) map[string]*ResourceURIBuilder {
	result := make(map[string]*ResourceURIBuilder)
	for name := range a.resources {
		result[name] = a.ResourceURIBuilder(name, c)
	}
	return result
}

func (a *Webapp) ResourceURIBuilder(resourceName string, c echo.Context) *ResourceURIBuilder {
	resource := a.resources[resourceName]
	return &ResourceURIBuilder{a, c,
		resource.Resource, ResourceURIs{resource.URIPattern}}
}

func ReqLog(c echo.Context) *logrus.Entry {
	l, ok := c.Get(requestLoggerKey).(*logrus.Entry)
	if !ok {
		l = logrus.NewEntry(log)
		c.Set(requestLoggerKey, l)
	}
	return l
}
