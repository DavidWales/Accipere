package appframe

import (
	"github.com/labstack/echo"
)

// This interface is, if implemented, called by RegisterResource to validate
// that the resource instance is in a valid state.  Implementations should
// panic on failure.
type ResourceObjectValidator interface {
	CheckResourceObject()
}

type ResourceIndexer interface {
	ResourceIndex(echo.Context) error
}

type ResourceCreator interface {
	CreateResource(echo.Context) error
}

type ResourceCreationFormProvider interface {
	ResourceCreator
	GetNewResourceForm(echo.Context) error
}

type ResourceShower interface {
	ShowResource(echo.Context) error
}

func ShowResourceHandler(o ResourceShower) echo.HandlerFunc {
	return o.ShowResource
}

type ResourceEditor interface {
	GetResourceEditingForm(echo.Context) error
	UpdateResource(echo.Context) error
}

type ResourceDeleter interface {
	DeleteResource(echo.Context) error
}

