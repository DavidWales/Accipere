package appframe

import (
	"encoding/json"
)

type jsonObj map[string]interface{}

func (o jsonObj) PairStream() <-chan struct{ Key string; Value interface{} } {
	resultChan := make(chan struct{ Key string; Value interface{} })
	go func() {
		for k, v := range o {
			resultChan <- struct{ Key string; Value interface{} } {
				Key: k,
				Value: v,
			}
			close(resultChan)
		}
	}()
	return resultChan
}

type partialJsonObjUnmarshaller map[string]json.RawMessage

func chopRight(s string, n int) string {
	if len(s) <= n {
		return ""
	}
	return s[:len(s) - n]
}
