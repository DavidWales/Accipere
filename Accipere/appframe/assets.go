package appframe

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	
	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
)

type AssetMapping struct {
	VirtualPath string
	FilePath string
	//TODO Recursive bool
}

// Relative FilePaths in mappings are interpreted relative to the current
// working directory.
func (a *Webapp) MapAssets(mappings ...AssetMapping) *Webapp {
	if a.assets == nil {
		a.assets = make(map[string]AssetInfo)
	}
	if a.assetURIs == nil {
		a.assetURIs = make(map[string]struct{})
	}
	for _, m := range mappings {
		vpath := m.VirtualPath
		fpath, err := filepath.Abs(m.FilePath)
		if err != nil { panic(err) }
		files, err := ioutil.ReadDir(fpath)
		if err != nil { panic(err) }
		for _, file := range files {
			if file.IsDir() { continue }
			
			// Split filename base and extension
			base, ext := splitFileBaseAndExt(file.Name())
			
			// Compute hash of file contents
			fileFullpath := filepath.Join(fpath, file.Name())
			contentHash := hashOfContents(fileFullpath)
			
			// Add entry to a.assets
			hasherizedPath := fmt.Sprintf(
				"%v/%v-%v%v",
				vpath, base, contentHash, ext,
			)
			a.assets[vpath + "/" + file.Name()] = AssetInfo{
				PublicName: hasherizedPath,
			}
			a.assetURIs[hasherizedPath] = struct{}{}
			
			a.Echo.File(hasherizedPath, fileFullpath)
			log.WithFields(logrus.Fields{
				"virtualPath": hasherizedPath,
				"physicalPath": fileFullpath,
			}).Info("Asset mapping added")
		}
	}
	return a
}

func splitFileBaseAndExt(name string) (base string, ext string) {
	ext = filepath.Ext(name)
	base = strings.TrimSuffix(name, ext)
	return
}

// From http://www.mrwaggel.be/post/generate-md5-hash-of-a-file/
func hashOfContents(filepath string) string {
	file, err := os.Open(filepath)
	if err != nil { panic(err) }
	defer file.Close()
	
	hash := md5.New()
	
	if _, err := io.Copy(hash, file); err != nil {
		panic(err)
	}
	
	hashInBytes := hash.Sum(nil)[:16]
	
	return hex.EncodeToString(hashInBytes)
}

func (a *Webapp) IsAssetRequest(c echo.Context) bool {
	_, ok := a.assetURIs[c.Path()]
	return ok
}
