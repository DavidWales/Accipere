package appframe

import (
	"fmt"
	"html/template"
	"io"
	"path/filepath"
	"time"
	
	"github.com/labstack/echo"
	"github.com/Sirupsen/logrus"
)

type PageContext struct {
	context echo.Context
	Resources map[string]*ResourceURIBuilder
	Page interface{}
}

type FlashMessages struct {
	Errors []string
	Warnings []string
	Infos []string
	Success string
}

func (pc *PageContext) Messages() *FlashMessages {
	flash := pc.context.Get(FlashContextKey)
	if flash == nil {
		return nil
	}
	if flash, ok := flash.(*FlashMessages); ok {
		return flash
	}
	return nil
}

type NamedObjects map[string]interface{}

func (a *Webapp) WithTemplateFuncs(funcs FuncMap) *Webapp {
	if a.TemplateFuncs == nil {
		a.TemplateFuncs = make(map[string]interface{}, len(funcs))
	}
	for name, fn := range funcs {
		a.TemplateFuncs[name] = fn
	}
	return a
}

type FuncMap map[string]interface{}

// The target is returned to allow chaining
func (a *Webapp) LoadTemplates(dir string) *Webapp {
	topic := "setup"
	fnlog := logrus.NewEntry(logrus.StandardLogger())
	defer func() {
		if err := recover(); err != nil {
			fnlog.WithField("topic", topic).Fatal(err)
		}
		
		return
	}()
	baselog := fnlog
	
	tmplFuncs := template.FuncMap{
		"assoc": func(kvPairs ...interface{}) map[string]interface{} {
			result := make(map[string]interface{}, len(kvPairs) / 2)
			for i, l := 0, len(kvPairs) - 1; i < l; i += 2 {
				result[fmt.Sprintf("%v", kvPairs[i])] = kvPairs[i+1]
			}
			return result
		},
		"asset": func(name string) string {
			return a.assets[name].PublicName
		},
		"csrf_token": func(pc *PageContext) template.HTML {
			return template.HTML(fmt.Sprintf(
				`<input type="hidden" name="%v" value="%v" />`,
				CSRFFormFieldName,
				pc.context.Get(CSRFContextKey),
			))
		},
	}
	for name, fn := range a.TemplateFuncs {
		tmplFuncs[name] = fn
	}
	
	startTime := time.Now()
	// Get the list of layout files
	topic = "Reading layout list"
	layouts, err := filepath.Glob(filepath.Join(dir, "layouts", "*" + templateExt))
	if err != nil { panic(err) }
	
	// Get the list of partials
	topic = "Reading partials list"
	partials, err := filepath.Glob(filepath.Join(dir, "partials", "*" + templateExt))
	if err != nil { panic(err) }
	
	// Get the list of view files
	topic = "Reading view list"
	views, err := filepath.Glob(filepath.Join(dir, "views", "*" + templateExt))
	if err != nil { panic(err) }
	
	partialsTmpl := template.New("templates").Funcs(tmplFuncs)
	if len(partials) > 0 {
		topic = "Parsing partials"
		template.Must(partialsTmpl.ParseFiles(partials...))
	}
	
	for _, layout := range layouts {
		fnlog := baselog.WithField("layoutFile", layout)
		
		topic = "Cloning partials for layout"
		layoutTmpl := template.Must(partialsTmpl.Clone())
		layoutName := chopRight(filepath.Base(layout), len(templateExt))
		
		topic = "Parsing layout"
		template.Must(layoutTmpl.ParseFiles(layout))
		
		// TODO See if there is any way to only parse the views once, then
		// compute the cross product with the layouts.
		for _, view := range views {
			fnlog := fnlog.WithField("viewFile", view)
			
			topic = "Cloning layout for view"
			viewTmpl := template.Must(layoutTmpl.Clone())
			
			viewName := filepath.Base(view)
			if layoutName != "normal" {
				viewName = layoutName + "/" + viewName
			}
			
			topic = "Parsing view"
			a.templates[viewName] = template.Must(viewTmpl.ParseFiles(view))
			
			fnlog.WithFields(logrus.Fields{
				"view": viewName,
			}).Info("view loaded")
		}
	}
	log.WithFields(logrus.Fields{
		"elapsedTime": time.Now().Sub(startTime),
		"dir": dir,
	}).Info("Templates loaded")
	return a
}

func (a *Webapp) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	fnlog := ReqLog(c).WithFields(logrus.Fields{
		"template": name,
	})
	tmpl, ok := a.templates[name]
	if !ok {
		fnlog.Error("Template does not exist")
		return fmt.Errorf("missing template %q", name)
	}
	fnlog.Info("Responding with rendered template")
	return tmpl.ExecuteTemplate(w, "page", &PageContext{
		context: c,
		Resources: a.resourceURIBuilders(c),
		Page: data,
	})
}
