package appframe

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	
	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type Session interface {
	ReadSessionState(interface{}) error
	WriteSessionState(interface{}) error
	SaveSessionState(echo.Context) error
	Terminate(echo.Context) error
}

type SessionMiddlewareConfig struct {
	Skipper middleware.Skipper
	LoadSession func(echo.Context) (Session, error)
	ContextKey string
	FlashKey string
	
	// If anything other than the default FlashMessages type is used for this
	// the functions designed to manipulate the flash messages will not work
	// with the exception of the SendFlash
	FlashType reflect.Type
}

var ErrNoFlash = errors.New("Flash message object not available")

var DefaultSessionMiddlewareConfig = SessionMiddlewareConfig{
	Skipper: middleware.DefaultSkipper,
	
	// Key used to store the session in the echo.Context.
	ContextKey: "session",
	
	// Key used to store the flash in the top level of the session state.
	FlashKey: "flash",
	
	FlashType: reflect.TypeOf(FlashMessages{}),
}

func (a *Webapp) SessionMiddleware(config SessionMiddlewareConfig) echo.MiddlewareFunc {
	if config.LoadSession == nil {
		panic(fmt.Errorf("Session middleware must configure LoadSession"))
	}
	
	defVals := &DefaultSessionMiddlewareConfig
	
	if config.Skipper == nil {
		config.Skipper = defVals.Skipper
	}
	if len(config.ContextKey) == 0 {
		config.ContextKey = defVals.ContextKey
	}
	if len(config.FlashKey) == 0 {
		config.FlashKey = defVals.FlashKey
	}
	if config.FlashType == nil {
		config.FlashType = defVals.FlashType
	}
	
	a.sessionContextKey = config.ContextKey
	
	return func (hf echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if !a.IsAssetRequest(c) && !config.Skipper(c) {
				// Call session loader
				session, err := config.LoadSession(c)
				flash := &flashLoader{&config, reflect.New(config.FlashType).Interface()}
				var flasher *flashRecorder
				if err == nil {
					c.Set(config.ContextKey, session)
					
					// Check for flash in session
					err = session.ReadSessionState(flash)
					
					// Clear flash from session
					session.WriteSessionState(jsonObj{
						config.FlashKey: &jsonObj{},
					})
				}
				if err == nil {
					c.Set(FlashContextKey, flash.Messages)
					if messages, ok := flash.Messages.(*FlashMessages); ok {
						flasher = &flashRecorder{
							session,
							config.FlashKey,
							messages,
							new(FlashMessages),
						}
						c.Set(FlashMessengerContextKey, flasher)
					}
					c.Set(FlashSetterContextKey, func(flash interface{}) error {
						// If flash is explicitly set, don't allow flasher messages
						flasher = nil
						return session.WriteSessionState(jsonObj{
							config.FlashKey: flash,
						})
					})
				} else {
					session = nil
				}
				defer func() {
					flasher.SaveToSession()
					if session != nil {
						session.SaveSessionState(c)
					}
				}()
			}
			
			return hf(c)
		}
	}
}

type flashLoader struct {
	*SessionMiddlewareConfig
	Messages interface{}
}

func (fl *flashLoader) UnmarshalJSON(data []byte) (err error) {
	var o partialJsonObjUnmarshaller
	err = json.Unmarshal(data, &o)
	if err != nil {
		return
	}
	if flashData, ok := o[fl.FlashKey]; ok {
		err = json.Unmarshal(flashData, fl.Messages)
	}
	return
}

type flashRecorder struct {
	Session Session
	FlashKey string
	OldMessages *FlashMessages
	NewMessages *FlashMessages
}

func getFlashRecorder(c echo.Context) *flashRecorder {
	flasher := c.Get(FlashMessengerContextKey)
	if result, ok := flasher.(*flashRecorder); ok {
		return result
	}
	return nil
}

// Overwrite the contents of the forwarded flash with the received flash.
func Reflash(c echo.Context) error {
	flasher := getFlashRecorder(c)
	if flasher == nil {
		return ErrNoFlash
	}
	*flasher.NewMessages = *flasher.OldMessages
	return nil
}

func badFlash(c echo.Context, msgType, message string) {
	ReqLog(c).WithFields(logrus.Fields{
		"top": "flash message",
		"msgType": msgType,
		"message": message,
	}).Error("No session support enabled")
}

func FlashError(c echo.Context, message string) {
	flasher := getFlashRecorder(c)
	if flasher == nil {
		badFlash(c, "error", message)
		return
	}
	flasher.NewMessages.Errors = append(flasher.NewMessages.Errors, message)
}

func FlashWarning(c echo.Context, message string) {
	flasher := getFlashRecorder(c)
	if flasher == nil {
		badFlash(c, "warning", message)
		return
	}
	flasher.NewMessages.Warnings = append(flasher.NewMessages.Warnings, message)
	return
}

func FlashInfo(c echo.Context, message string) {
	flasher := getFlashRecorder(c)
	if flasher == nil {
		badFlash(c, "info", message)
		return
	}
	flasher.NewMessages.Infos = append(flasher.NewMessages.Infos, message)
	return
}

func FlashSuccess(c echo.Context, message string) {
	flasher := getFlashRecorder(c)
	if flasher == nil {
		badFlash(c, "success", message)
		return
	}
	flasher.NewMessages.Success = message
	return
}

func (f *flashRecorder) SaveToSession()  {
	if f == nil {
		return
	}
	jsonData, errLog := json.Marshal(f.NewMessages)
	if errLog == nil {
		log.WithField("flashMessages", string(jsonData)).Debug("Saving flash to session")
	} else {
		log.WithError(errLog).Debug("Saving flash to session")
	}
	
	//err := f.Session.WriteSessionState(f.NewMessages)
	err := f.Session.WriteSessionState(jsonObj{
		f.FlashKey: f.NewMessages,
	})
	if err != nil {
		log.WithField("topic", "Saving flash to session").Error(err)
	}
}

func SetFlash(c echo.Context, flash interface{}) {
	flashSetter := c.Get(FlashSetterContextKey)
	if flashSetter == nil {
		ReqLog(c).
			WithField("topic", "Setting flash").
			Error("No session support available")
		return
	}
	if flashSetter, ok := flashSetter.(func(interface{}) error); ok {
		err := flashSetter(flash)
		if err != nil {
			ReqLog(c).
				WithField("topic", "Setting flash").
				Error(err)
		}
	}
}

func (a *Webapp) Session(c echo.Context) Session {
	if len(a.sessionContextKey) == 0 {
		return nil
	}
	session := c.Get(a.sessionContextKey)
	if session, ok := session.(Session); ok {
		return session
	}
	return nil
}
