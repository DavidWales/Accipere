package appframe

import (
	"bytes"
	"fmt"
	"net/url"
	
	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
)

type ErrResourceNameTaken string

func (e ErrResourceNameTaken) Error() string {
	return fmt.Sprintf("Resource name %q already used", e)
}

func (a *Webapp) RegisterResource(baseURI string, resource interface{}, name string) {
	if r, ok := resource.(ResourceObjectValidator); ok {
		r.CheckResourceObject()
	}
	uris := ResourceURIs{baseURI}
	supportedActions := make([]string, 0, 6)
	if r, ok := resource.(ResourceIndexer); ok {
		a.Echo.GET(baseURI, r.ResourceIndex)
		supportedActions = append(supportedActions, "index")
	}
	if r, ok := resource.(ResourceCreationFormProvider); ok {
		a.Echo.GET(uris.CreateURI(), r.GetNewResourceForm)
		supportedActions = append(supportedActions, "new")
	}
	if r, ok := resource.(ResourceCreator); ok {
		a.Echo.POST(uris.CreateURI(), r.CreateResource)
		supportedActions = append(supportedActions, "create")
	}
	if r, ok := resource.(ResourceShower); ok {
		a.Echo.GET(uris.InstanceURI(), r.ShowResource)
		supportedActions = append(supportedActions, "show")
	}
	if r, ok := resource.(ResourceEditor); ok {
		a.Echo.GET(uris.EditURI(), r.GetResourceEditingForm)
		a.Echo.POST(uris.EditURI(), r.UpdateResource)
		supportedActions = append(supportedActions, "edit")
	}
	if r, ok := resource.(ResourceDeleter); ok {
		a.Echo.DELETE(uris.InstanceURI(), r.DeleteResource)
		supportedActions = append(supportedActions, "delete")
	}
	log.WithFields(logrus.Fields{
		"baseURI": baseURI,
		"actions": supportedActions,
	}).Infof("%T registered", resource)
	if len(name) > 0 {
		if a.resources == nil {
			a.resources = make(ResourcesMap)
		} else if _, bad := a.resources[name]; bad {
			panic(ErrResourceNameTaken(name))
		}
		
		a.resources[name] = ResourceInfo{
			Resource: resource,
			URIPattern: baseURI,
		}
	}
}

type ResourceURIs struct {
	BaseURI string
}

func (b ResourceURIs) CollectionURI() string { return b.BaseURI }
func (b ResourceURIs) CreateURI() string { return b.BaseURI + "/new" }
func (b ResourceURIs) InstanceURI() string { return b.BaseURI + "/:id" }
func (b ResourceURIs) EditURI() string { return b.InstanceURI() + "/edit" }

// The methods of this type reuse the matching parameter segments (although the
// assigned names need not match) from the URI of the current request.  This
// allows referencing "up" to a master model or, if additional parameters are
// provided, "down" to a detail.  Moving horizontally in the URI tree space
// is also allowed, using the same set of URI param values as far as the URI
// patterns are the same between the resources, then going to addnlParams.
type ResourceURIBuilder struct {
	app *Webapp
	context echo.Context
	resource interface{}
	uris ResourceURIs
}

var (
	ErrURIBuilderNotStringable = fmt.Errorf("ResourceURIBuilder cannot render")
)

// Implement Stringer to panic so we can see misuse of a URI builder.
func (b *ResourceURIBuilder) String() string {
	panic(ErrURIBuilderNotStringable)
}

// See the ResourceURIBuilder type for note on what parameter values are used
// in generating the URI.
func (b *ResourceURIBuilder) Collection(addnlParams ...interface{}) string {
	return b.makeURI(b.uris.CollectionURI(), addnlParams)
}

func (b *ResourceURIBuilder) CreateInstance(addnlParams ...interface{}) string {
	return b.makeURI(b.uris.CreateURI(), addnlParams)
}

// See the ResourceURIBuilder type for note on what parameter values are used
// in generating the URI.
func (b *ResourceURIBuilder) Instance(addnlParams ...interface{}) string {
	return b.makeURI(b.uris.InstanceURI(), addnlParams)
}

// See the ResourceURIBuilder type for note on what parameter values are used
// in generating the URI.
func (b *ResourceURIBuilder) EditInstance(addnlParams ...interface{}) string  {
	return b.makeURI(b.uris.EditURI(), addnlParams)
}

func (b *ResourceURIBuilder) makeURI(resourcePath string, addnlParams []interface{}) string {
	c := b.context
	matchedPath := c.Path()
	
	mi, ml := 0, len(matchedPath)
	ri, rl := 0, len(resourcePath)
	nscp := 0 // number of shared context parameters
	for ; mi < ml && ri < rl; mi, ri = mi+1, ri+1 {
		if matchedPath[mi] != resourcePath[ri] {
			break
		}
		if matchedPath[mi] == ':' {
			nscp++
			// Advance mi past param in matchedPath
			for mi < ml && matchedPath[mi] != '/' { mi++ }
			// Advance ri past param in resourcePath
			for ri < rl && resourcePath[ri] != '/' { ri++ }
		}
	}
	ri = 0
	uri := new(bytes.Buffer)
	allParams := make([]interface{}, nscp + len(addnlParams))
	// TODO if addnlParams[0] looks like ".." or "../..", etc., decrement
	// nscp by the number of ".." groups.
	for i := 0; i < nscp; i++ {
		allParams[i] = c.ParamValues()[i]
	}
	for i, l := 0, len(addnlParams); i < l; i++ {
		allParams[nscp + i] = addnlParams[i]
	}
	np := 0
	for ; ri < rl; ri++ {
		if resourcePath[ri] == ':' {
			// Inject parameter value
			uri.WriteString(url.PathEscape(fmt.Sprintf("%v", allParams[np])))
			np++
			
			// Skip remainder of param in resourcePath
			for ri < rl && resourcePath[ri] != '/' { ri++ }
			if ri < rl {
				uri.WriteByte(resourcePath[ri])
			}
		} else {
			// Copy one byte
			uri.WriteByte(resourcePath[ri])
		}
	}
	return uri.String()
}
