package accipere_dbtest

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	
	"github.com/google/subcommands"
	app_db "../accipere_db"
)

var (
	Db app_db.DBAccess
)

func RegisterSubcommands() {
	subcommands.Register(&loginExternalAccountCommand{}, "login")
	subcommands.Register(&createLocalAccountCommand{}, "")
	subcommands.Register(&loginLocalAccountCommand{}, "login")
	subcommands.Register(&accessLoginSessionCommand{}, "")
	subcommands.Register(&saveReceiptCommand{}, "receipts")
	subcommands.Register(&getReceiptListCommand{}, "receipts")
	subcommands.Register(&getCustomReceiptListCommand{}, "receipts")
	subcommands.Register(&getReceiptImageCommand{}, "receipts")
	subcommands.Register(&setReceiptTotalCommand{}, "receipts")
	subcommands.Register(&deleteReceiptCommand{}, "receipts")
}

type commandArgsError interface {
	IAmAUsageError()
}

func handleCommandPanic(errResult *subcommands.ExitStatus) {
	err := recover()
	if err == nil {
		return
	}
	if _, aok := err.(commandArgsError); aok {
		*errResult = subcommands.ExitUsageError
	} else {
		*errResult = subcommands.ExitFailure
	}
	log.Println(err)
}

func commandArgsExpected(f *flag.FlagSet, count int) []string {
	result := f.Args()
	if len(result) != count {
		panic(&commandArgsCountError{count, len(result)})
	}
	return result
}

type commandArgsCountError struct {
	expected, actual int
}

func (e commandArgsCountError) Error() string {
	return fmt.Sprintf(
		"expected %v arguments, got %v\n",
		e.expected,
		e.actual,
	)
}

func (e commandArgsCountError) IAmAUsageError() {}

func argMustAtoi(arg string) int {
	result, err := strconv.Atoi(arg)
	if err != nil {
		panic(&commandFormatError{err})
	}
	return result
}

type commandFormatError struct {
	source error
}

func (e commandFormatError) Error() string { return e.source.Error() }
func (e commandFormatError) IAmAUsageError() {}
