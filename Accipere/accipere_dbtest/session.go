package accipere_dbtest

import (
	"context"
	"encoding/base64"
	"flag"
	"fmt"
	"strings"
	"unicode"
	
	"github.com/google/subcommands"
)

type accessLoginSessionCommand struct {
}

func (*accessLoginSessionCommand) Name() string { return "accessSess" }
func (*accessLoginSessionCommand) Synopsis() string { return "Access a session, printing the user ID." }
func (*accessLoginSessionCommand) Usage() string {
	return `accessSess <session-token>
	Accesses the session (if the session is still valid), updates the
	last access time of the session, and prints out the user ID of the
	associated user.
`
}

func (*accessLoginSessionCommand) SetFlags(*flag.FlagSet) {}

func (c *accessLoginSessionCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 1)
	
	session_token, err := base64.StdEncoding.DecodeString(
		stripFrom(args[0], unicode.IsSpace),
	)
	if err != nil { panic(err) }
	
	userId, err := Db.AccessLoginSession(ctx, session_token)
	if err != nil { panic(err) }
	fmt.Printf("Accessed session for user %v\n", userId)
	return subcommands.ExitSuccess
}

// http://stackoverflow.com/a/32081891/160072
func stripFrom(str string, pred func (rune) bool) string {
	return strings.Map(func(r rune) rune {
		if pred(r) { return -1 }
		return r
	}, str)
}

