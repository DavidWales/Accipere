package accipere_dbtest

import (
	"context"
	"flag"
	"fmt"
	
	"github.com/google/subcommands"
	"github.com/howeyc/gopass"
)

type createLocalAccountCommand struct {
}

func (*createLocalAccountCommand) Name() string { return "createAcct" }
func (*createLocalAccountCommand) Synopsis() string { return "Create a locally authenticated account." }
func (*createLocalAccountCommand) Usage() string {
	return `createAcct <email> <name>
	Create a locally authenticated account with the given email and name.  A
	password will be prompted.
`
}
func (*createLocalAccountCommand) SetFlags(*flag.FlagSet) {}

func (c *createLocalAccountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 2)
	
	email := args[0]
	name := args[1]
	
	fmt.Print("Password: ")
	passwd, err := gopass.GetPasswd()
	if err != nil { panic(err) }
	
	userId, err := Db.CreateLocalAccount(ctx, email, name, string(passwd))
	if err != nil { panic(err) }
	fmt.Printf("User created with ID %v\n", userId)
	return subcommands.ExitSuccess
}
