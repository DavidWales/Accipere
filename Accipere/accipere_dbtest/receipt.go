package accipere_dbtest

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
	"time"
	
	"github.com/google/subcommands"
	"github.com/shopspring/decimal"
	log "github.com/Sirupsen/logrus"
	app_db "../accipere_db"
	geo "../accipere_geo"
)

////////// Save Receipt //////////

type saveReceiptCommand struct {
	location string
}

func (*saveReceiptCommand) Name() string { return "save" }
func (*saveReceiptCommand) Synopsis() string { return "Save a receipt image for a user." }
func (*saveReceiptCommand) Usage() string {
	return `save <user-id> <image-filename>
	Read the given file and save it into the database as a new receipt linked
	to the account of the given user.  The ID of the new receipt will be
	printed.
`
}

func (c *saveReceiptCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.location, "loc", "", `Give "longitude,latitude" of receipt`)
}

func (c *saveReceiptCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 2)
	
	userId := argMustAtoi(args[0])
	
	location, err := geo.ParseLocation(c.location)
	if err != nil { panic(&commandFormatError{err}) }
	
	image, err := ioutil.ReadFile(args[1])
	if err != nil { panic(err) }
	
	receiptId, err := Db.SaveReceipt(ctx, userId, location, image)
	if err != nil { panic(err) }
	
	fmt.Printf("Saved receipt ID: %v\n", receiptId)
	return subcommands.ExitSuccess
}

////////// Get Receipt List //////////

type getReceiptListCommand struct {
	count int
	start int
}

func (*getReceiptListCommand) Name() string { return "listReceipts" }
func (*getReceiptListCommand) Synopsis() string { return "Print a list of receipt record info" }
func (*getReceiptListCommand) Usage() string {
	return `listReceipts <user-id>
	List the non-image receipt information for the specified user.
`
}

func (c *getReceiptListCommand) SetFlags(f *flag.FlagSet) {
	f.IntVar(&c.count, "n", 0, "max number of rows to return")
	f.IntVar(&c.start, "from", 0, "number of rows to skip")
}

func (c *getReceiptListCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 1)
	
	userId := argMustAtoi(args[0])
	
	var page *app_db.Page
	if c.count > 0 {
		page = &app_db.Page{c.start, c.count}
	}
	
	receipts, err := Db.GetReceiptList(ctx, userId, page)
	if err != nil { panic(err) }
	
	fmt.Printf(
		"%-11v   %-11v   %-10v   %-28v   %-10v\n",
		"ID",
		"Account ID",
		"Date",
		"Location",
		"Total",
	)
	fmt.Printf(
		"%-11v   %-11v   %-10v   %-28v   %-10v\n",
		"-----------",
		"-----------",
		"----------",
		"----------------------------",
		"----------",
	)
	vals := make([]interface{}, 0, 5)
	for _, r := range receipts {
		vals = vals[:0]
		vals = append(vals, r.Id)
		if r.Account_id.Valid {
			vals = append(vals, int(r.Account_id.Int64))
		} else {
			vals = append(vals, "")
		}
		vals = append(vals, r.Date.Format(time.RFC3339)[:10])
		if r.Location.Valid {
			vals = append(vals, r.Location.LongLat.String())
		} else {
			vals = append(vals, "")
		}
		if r.Total.Valid {
			vals = append(vals, r.Total.Decimal)
		} else {
			vals = append(vals, "")
		}
		fmt.Printf("%11d   %11v   %-10v   %-28s   %10v\n", vals...)
	}
	return subcommands.ExitSuccess
}

////////// Get Customized Receipt List //////////

type getCustomReceiptListCommand struct {
	count int
	start int
	location string
	amountRange string
}

func (*getCustomReceiptListCommand) Name() string { return "listReceiptsDist" }
func (*getCustomReceiptListCommand) Synopsis() string { return "Print a list of receipt record info with distance" }
func (*getCustomReceiptListCommand) Usage() string {
	return `listReceipts <user-id>
	List the non-image receipt information for the specified user, including
	distance from a specified point.
`
}

func (c *getCustomReceiptListCommand) SetFlags(f *flag.FlagSet) {
	f.IntVar(&c.count, "n", 0, "max number of rows to return")
	f.IntVar(&c.start, "from", 0, "number of rows to skip")
	f.StringVar(&c.location, "loc", "-88.224807,40.113625", "reference location")
	f.StringVar(&c.amountRange, "amount", "..", "range of amounts ([min]..[max])")
}

func (c *getCustomReceiptListCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 1)
	
	userId := argMustAtoi(args[0])
	
	var page *app_db.Page
	if c.count > 0 {
		page = &app_db.Page{c.start, c.count}
	}
	
	refLoc, err := geo.ParseLocation(c.location)
	if err != nil { panic(err) }
	
	cust := &app_db.QueryCustomization{
		Filter: makeAmountFilter(c.amountRange),
		Ordering: &app_db.OrderByDistance{string(app_db.RFLocation), refLoc},
		ExtraResultColumns: app_db.ResultColumns(
			&app_db.DistanceFromPoint{string(app_db.RFLocation), refLoc},
		),
	}
	
	receipts, err := Db.GetCustomizedReceiptList(ctx, userId, cust, page)
	if err != nil { panic(err) }
	
	if len(cust.ExtraResultColumns) < 1 {
		log.WithFields(log.Fields{
			"colCount": len(cust.ExtraResultColumns),
		}).Fatal("Expected more extra result columns")
	}
	if len(cust.ExtraResultColumns[0].OutputValues) < len(receipts) {
		log.WithFields(log.Fields{
			"receiptCount": len(receipts),
			"erCount": len(cust.ExtraResultColumns[0].OutputValues),
		}).Fatal("Expected more results in extra result column")
	}
	
	fmt.Printf(
		"%-11v   %-11v   %-10v   %-28v   %-10v   %-20v\n",
		"ID",
		"Account ID",
		"Date",
		"Location",
		"Total",
		"Distance",
	)
	fmt.Printf(
		"%-11v   %-11v   %-10v   %-28v   %-10v   %-20v\n",
		"-----------",
		"-----------",
		"----------",
		"----------------------------",
		"----------",
		"--------------------",
	)
	vals := make([]interface{}, 0, 5)
	for i, r := range receipts {
		vals = vals[:0]
		vals = append(vals, r.Id)
		if r.Account_id.Valid {
			vals = append(vals, int(r.Account_id.Int64))
		} else {
			vals = append(vals, "")
		}
		vals = append(vals, r.Date.Format(time.RFC3339)[:10])
		if r.Location.Valid {
			vals = append(vals, r.Location.LongLat.String())
		} else {
			vals = append(vals, "")
		}
		if r.Total.Valid {
			vals = append(vals, r.Total.Decimal)
		} else {
			vals = append(vals, "")
		}
		vals = append(vals, cust.GetExtraResult(i, 0))
		fmt.Printf("%11d   %11v   %-10v   %-28s   %10v   %20v\n", vals...)
	}
	return subcommands.ExitSuccess
}

func makeAmountFilter(rng string) *app_db.FilterByRange {
	rngParts := strings.SplitN(rng, "..", 3)
	if len(rngParts) != 2 {
		panic(fmt.Errorf("Amount range must by [min]..[max]"))
	}
	var minVal, maxVal interface{}
	var err error
	if len(rngParts[0]) > 0 {
		minVal, err = decimal.NewFromString(rngParts[0])
		if err != nil { panic(err) }
	}
	if len(rngParts[1]) > 0 {
		maxVal, err = decimal.NewFromString(rngParts[1])
		if err != nil { panic(err) }
	}
	return &app_db.FilterByRange{
		ExprOfTuple: string(app_db.RFTotal),
		RangeStart: minVal, RangeStartOpen: false,
		RangeEnd: maxVal, RangeEndOpen: false,
	}
}

////////// Get Receipt Image //////////

type getReceiptImageCommand struct {
}

func (*getReceiptImageCommand) Name() string { return "getReceipt" }
func (*getReceiptImageCommand) Synopsis() string { return "Retrieve a receipt image stored in the database" }
func (*getReceiptImageCommand) Usage() string {
	return `getReceipt <user-id> <receipt-id> <outfile>
	The binary content of the receipt will written to the output file.
`
}

func (*getReceiptImageCommand) SetFlags(f *flag.FlagSet) {
}

func (c *getReceiptImageCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 3)
	
	userId := argMustAtoi(args[0])
	receiptId := argMustAtoi(args[1])
	outfile := args[2]
	
	image, err := Db.GetReceiptImage(ctx, userId, receiptId)
	if err != nil { panic(err) }
	
	err = ioutil.WriteFile(outfile, image, 0600)
	if err != nil { panic(err) }
	
	return subcommands.ExitSuccess
}

////////// Set Receipt Total //////////

type setReceiptTotalCommand struct {
}

func (*setReceiptTotalCommand) Name() string { return "setReceiptTotal" }
func (*setReceiptTotalCommand) Synopsis() string { return "" }
func (*setReceiptTotalCommand) Usage() string {
	return `setReceiptTotal <user-id> <receipt-id> <total>
`
}

func (*setReceiptTotalCommand) SetFlags(f *flag.FlagSet) {}
func (*setReceiptTotalCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 3)
	
	userId := argMustAtoi(args[0])
	receiptId := argMustAtoi(args[1])
	total := args[2]
	
	err := Db.SetReceiptFields(ctx, userId, receiptId,
		app_db.ReceiptFieldAssignment{app_db.RFTotal, total},
	)
	if err != nil { panic(err) }
	return subcommands.ExitSuccess
}

////////// Delete Receipt //////////

type deleteReceiptCommand struct {
}

func (*deleteReceiptCommand) Name() string { return "deleteReceipt" }
func (*deleteReceiptCommand) Synopsis() string { return "Delete a receipt from the database." }
func (*deleteReceiptCommand) Usage() string {
	return `deleteReceipt <user-id> <receipt-id>
	Deletes the receipt identified by user ID and receipt ID.  If both values
	do not match any record, logs an error.
`
}

func (*deleteReceiptCommand) SetFlags(f *flag.FlagSet) {}

func (c *deleteReceiptCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 2)
	
	userId := argMustAtoi(args[0])
	receiptId := argMustAtoi(args[1])
	
	err := Db.DeleteReceipt(ctx, userId, receiptId)
	if err != nil { panic(err) }
	return subcommands.ExitSuccess
}
