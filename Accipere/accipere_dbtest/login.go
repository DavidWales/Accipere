package accipere_dbtest

import (
	"context"
	"encoding/base64"
	"flag"
	"fmt"
	
	"github.com/google/subcommands"
	"github.com/howeyc/gopass"
	auth "../accipere_auth"
)

////////// External Login //////////

type loginExternalAccountCommand struct {
}

func (*loginExternalAccountCommand) Name() string { return "loginExt" }
func (*loginExternalAccountCommand) Synopsis() string { return "Log in an externally authenticated user." }
func (*loginExternalAccountCommand) Usage() string {
	return `loginExt <email> <id> <name>:
	Log in an externally authenticated account, creating a user record if
	necessary, create a new session token, and print out the session token.
`
}

func (*loginExternalAccountCommand) SetFlags(*flag.FlagSet) {}

func (c *loginExternalAccountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 3)
	
	email := args[0]
	id := args[1]
	name := args[2]
	
	token, err := Db.CreateLoginSession(ctx, email, &auth.ExternalAccount{
		RealName: name,
		Id: id,
	})
	if err != nil { panic(err) }
	
	showToken(token)
	
	return subcommands.ExitSuccess
}

////////// Local Login //////////

type loginLocalAccountCommand struct {
}

func (*loginLocalAccountCommand) Name() string { return "loginLoc" }
func (*loginLocalAccountCommand) Synopsis() string { return "Log in a locally authenticated user." }
func (*loginLocalAccountCommand) Usage() string {
	return `loginLoc <email>
	Log in a locally authenticated account by validating password hash,
	creating a new session token, and print out the session token.  A password
	will be prompted.
`
}

func (*loginLocalAccountCommand) SetFlags(*flag.FlagSet) {}

func (c *loginLocalAccountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (exit subcommands.ExitStatus) {
	defer handleCommandPanic(&exit)
	args := commandArgsExpected(f, 1)
	
	email := args[0]
	
	fmt.Print("Password: ")
	passwd, err := gopass.GetPasswd()
	if err != nil { panic(err) }
	
	token, err := Db.CreateLoginSession(ctx, email, &auth.LocalAccount{string(passwd)})
	if err != nil { panic(err) }
	
	showToken(token)
	
	return subcommands.ExitSuccess
}

func showToken(token []byte) {
	fmt.Printf("token: %q\n", base64.StdEncoding.EncodeToString(token))
}

