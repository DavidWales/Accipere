#!/usr/bin/env ruby

require "pathname"
require "yaml"

module RelationalSchema
  class <<self
    def table_name(path)
      path.basename('.yaml')
    end
    
    def load_yaml(path)
      path.open('r') {|f| YAML.load(f)}
    end
    
    def col_names(table)
      table['columns'].map {|c| c['name']}
    end
    
    def table_schema(path)
      "#{table_name(path)}(#{col_names(load_yaml(path)).join(", ")})"
    end
    
    def main
      table_decls_dir = Pathname('database/structure/declarative')
      table_decls_dir.children("*.yaml").each do |table_path|
        puts table_schema(table_path)
      end
    end
  end
end

if __FILE__ == $0
  RelationalSchema.main
end
