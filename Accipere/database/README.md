## Setup

Here is what you will need to install on a machine in order to generate SQL schema upgrade scripts from the source files in this directory:

1. You need Ruby if you don't have it already -- version 2.1 or newer.
1. You need the `xmigra` gem.  Install it with:

    ```console
    $ gem install xmigra
    ```

## Database Setup

The PostgreSQL database needs to support PostGIS.  I installed PostgreSQL 9.5 and PostGIS 2.2 from the Ubuntu Xenial repositories.

If the server and GIS software are installed, then the following will have to be run in the database before the upgrade script:

```sql
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
```

## How to Generate an Upgrade script

From the directory containing this file, run:

```console
$ xmigra upgrade --production
```

(Leave off the `--production` flag when making an experimental build.  It is much safer to use it when upgrading a production server.)

The output of this command can be piped to `psql`.

## What Is This Program?

[XMigra](https://github.com/rtweeks/xmigra) is a database schema evolution management tool designed to work in cooperation with a version control system.  The wiki has a [tutorial](https://github.com/rtweeks/xmigra/wiki/Tutorial).  You can also learn more about the tool with:

```console
$ xmigra help
...
$ xmigra overview
...
```

